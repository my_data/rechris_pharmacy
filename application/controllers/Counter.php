<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Counter extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('employee');	
		$this->load->model('module');
		$this->load->model('drink');
		$this->load->library('cart');
		$this->load->model('report');
		$this->load->helper('function');
	}

	function index(){
		if(!$this->employee->is_logged_in()){
				redirect('login');
		}	
			$this->session->set_userdata('salemode','retail_sale');
			$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
			$data=array('page'=>'counter/dashboard','title'=>'Sales','allowed_modules'=>$allowed_modules);
			$this->load->view('template',$data);

	}

	function whole_sale(){
			if(!$this->employee->is_logged_in()){
					redirect('login');
			}
			$this->session->set_userdata('salemode','whole_sale');
		if (isset($_SESSION['salemode'])) {
			$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
			$data=array('page'=>'counter/whole_sale','title'=>'Whole Sale','allowed_modules'=>$allowed_modules);
			$this->load->view('template',$data);
			}
			else{
				redirect('counter');
			}
			
	}
	private function search_by_scanner(){
		$search=$this->uri->segment(3);
		$search=$this->db->escape_like_str($search);
		$search=(int)$search;
		$query="SELECT * FROM drinks WHERE item_id=$search  ORDER BY name ASC";
		$result=$this->drink->search($query);
		if (!$result) {
			$this->session->set_userdata('fail','No record Found Or Use barcode Scanner');
			redirect('counter');
		}
		if (isset($_SESSION['salemode'])) {
			$sale_mode=$this->session->userdata('salemode');
			if ($sale_mode=="retail_sale") {
				$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
				$data=array('page'=>'counter/dashboard','title'=>'Sales','allowed_modules'=>$allowed_modules,'result'=>$result);
				$this->load->view('template',$data);
			}
			if ($sale_mode=="whole_sale") {
				$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
				$data=array('page'=>'counter/whole_sale','title'=>'Sales','allowed_modules'=>$allowed_modules,'result'=>$result);
				$this->load->view('template',$data);
			}
		}
		else{
			redirect('counter');
		}
		
	}

	private function search_by_submit_form(){
		$search=$this->input->post('keyword');
		if (empty($search)) {
			$this->session->set_userdata('fail','Empty Search');
			redirect('counter');
		}
		$search= $this->db->escape_like_str($search);
		$query="SELECT * FROM drinks where match(name) AGAINST ('$search') ORDER BY name ASC";
		$result=$this->drink->search($query);

		if (!$result) {
			$this->session->set_userdata('fail','No Record Found');
			redirect('counter');
		}
		else{

			if (isset($_SESSION['salemode'])) {
				$sale_mode=$this->session->userdata('salemode');
				if ($sale_mode=="retail_sale") {
					$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
					$data=array('page'=>'counter/dashboard','title'=>'Sales','allowed_modules'=>$allowed_modules,'result'=>$result);
					$this->load->view('template',$data);
				}
				if ($sale_mode=="whole_sale") {
					$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
					$data=array('page'=>'counter/whole_sale','title'=>'Sales','allowed_modules'=>$allowed_modules,'result'=>$result);
					$this->load->view('template',$data);
				}
			}
			else{
				redirect('counter');
			}
		}
	}

	public function search(){
		if(!$this->employee->is_logged_in()){
			$this->session->set_userdata('fail','Please!! Login first');
			redirect('login');
		}
		if (isset($_POST['search'])) {
			$this->search_by_submit_form();
		}
		elseif (!empty($this->uri->segment(3))) {
		   $this->search_by_scanner();
		}
		else{
			redirect('counter');
		}
	}

	function add_to_cart(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['add'])) {
				if (isset($_SESSION['salemode'])) {
					$sale_mode=$_SESSION['salemode'];
					if ($sale_mode=="retail_sale") {
							$this->load->library('form_validation');
							$this->form_validation->set_rules('itemname','Item Name','required');
							$this->form_validation->set_rules('category','drink category','required');
							$this->form_validation->set_rules('stock','drink Amount','required');
							$this->form_validation->set_rules('qty','quantity','required');
							$this->form_validation->set_rules('price','price','required');
							$this->form_validation->set_rules('item_id','ID','required');
							$this->form_validation->set_rules('payment_type','payment_type','required');
							if ($this->form_validation->run()==TRUE) {
								$name=$this->input->post('itemname');
								$payment_type=$this->input->post('payment_type');
								$category=$this->input->post('category');
								$qty=(int)$this->input->post('qty');
								$stock=(int)$this->input->post('stock');
								$item_id=$this->input->post('item_id');
								$saler=$this->session->userdata('person_id');
								$new_stock=$stock-$qty;
								if ($new_stock<0){
									$this->session->set_userdata('fail','Ooops! Insuffitient Stock');
									redirect('counter');
								}
						//select payment type price
								$price=(int)$this->input->post('price');
								// if ($payment_type=='Cash') {
								// 	$price=$this->drink->get_cash_price($item_id);
								// }
								// if ($payment_type=='NHIF') {
								// 	$price=$this->drink->get_nhif_price($item_id);
								// }
								// if ($payment_type=='STRATEGY') {
								// 	$price=$this->drink->get_strategy_price($item_id);
								// }
								// if ($payment_type=='TANESCO') {
								// 	$price=$this->drink->get_tanesco_price($item_id);
								// }
								// if ($payment_type=='BUNGE') {
								// 	$price=$this->drink->get_binge_price($item_id);
								// }
								// if ($payment_type=='Others') {
								// 	$price=$this->drink->get_cash_price($item_id);
								// }
								$this->session->set_userdata('payment_type',$payment_type);
						//check if item exist first
								foreach ($this->cart->contents() as $drink) {
									if ($drink['id']==$item_id) {
										$item=array('rowid'=>$drink['rowid'],'qty'=>$qty,'price'=>$price,'category'=>$category,'stock'=>$stock);
										if($this->cart->update($item)){

											redirect('counter');
											
										}
									}
										}
								//add item to cart
								$name=str_replace(array( '(', ')' ), '-', $name);
								$name=str_replace(array( '%', '%' ), 'Percent', $name);
								$name=str_replace(array( '/', '/' ), '-', $name);
								$drink=array('id'=>$item_id,'name'=>$name,'qty'=>$qty,'price'=>$price,'category'=>$category,'stock'=>$stock,'payment_type'=>$payment_type);
										if($this->cart->insert($drink)){
											$this->session->set_userdata('success','Item Added Successfull');
										redirect('counter');
											
										}
										
									}
									if ($this->form_validation->run()==FALSE) {
										$this->session->set_userdata('fail','Fill all required');
										redirect('counter');	
									}
						}
					if ($sale_mode=="whole_sale") {
						$this->load->library('form_validation');
							$this->form_validation->set_rules('itemname','Item Name','required');
							$this->form_validation->set_rules('package','drink category','required');
							$this->form_validation->set_rules('package_amount','drink Amount','required');
							$this->form_validation->set_rules('qty','quantity','required');
							$this->form_validation->set_rules('wholesale_price','price','required');
							$this->form_validation->set_rules('item_id','ID','required');
							$this->form_validation->set_rules('wholesale_price','price','required');
							if ($this->form_validation->run()==TRUE) {
								$name=$this->input->post('itemname');
								$package=$this->input->post('package');
								$qty=(int)$this->input->post('qty');
								$package_amount=(int)$this->input->post('package_amount');
								$item_id=$this->input->post('item_id');
								$wholesale_price=$this->input->post('wholesale_price');
								$saler=$this->session->userdata('person_id');
								$new_stock=$package_amount-$qty;
								if ($new_stock<0){
									$this->session->set_userdata('fail','Ooops! Insuffitient Stock');
									redirect('counter/whole_sale');
								}
						//check if item exist first
								foreach ($this->cart->contents() as $drink) {
									if ($drink['id']==$item_id) {
										$item=array('rowid'=>$drink['rowid'],'qty'=>$qty,'price'=>$wholesale_price,'category'=>$package,'stock'=>$package_amount);
										if($this->cart->update($item)){

											redirect('counter/whole_sale');
											
										}
									}
										}
								//add item to cart
								$name=str_replace(array( '(', ')' ), '-', $name);
								$drink=array('id'=>$item_id,'name'=>$name,'qty'=>$qty,'price'=>$wholesale_price,'category'=>$package,'stock'=>$package_amount);
										if($this->cart->insert($drink)){
											$this->session->set_userdata('success','Item Added Successfull');
										redirect('counter/whole_sale');
											
										}
										
									}
									if ($this->form_validation->run()==FALSE) {
										$this->session->set_userdata('fail','Fill all required');
										redirect('counter/whole_sale');	
									}
					}
				}
		}
		else{
			redirect('counter');	
		}
		
	}

function remove_cart_item(){
	$rowid=$this->uri->segment(3);
	
	if($this->cart->remove($rowid)){
		$this->session->set_userdata('success','Item Removed Successfull');
		redirect('counter');
	}
}

function sale(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['sale'])) {
			if (isset($_SESSION['salemode'])) {
				$salemode=$this->session->userdata('salemode');
					if ($salemode=="retail_sale") {
							$payment_type=$_POST['payment_type'];
						if($this->cart->contents()){
						$sales=0;
						foreach ($this->cart->contents() as $item) {
							$name=$item['name'];
							$stock=$item['stock'];
							$item_id=(int)$item['id'];
							$qty=(int)$item['qty'];
							$cgry=$item['category'];
							$saler=$this->session->userdata('person_id');
							$sale_price=$item['price'];
							$total_price=($item['qty']*$sale_price);
							$new_stock=($stock-$qty);
							if ($new_stock<0) {
								$this->session->set_userdata('success','No Drugs Availabe');
								redirect('counter');
							}
							$new_stock=(int)$new_stock;
							$customer=$_POST['customer'];
							$time=date("Y-m-d",time());
							$query="INSERT INTO drinkSales VALUES('','$time','$name',$item_id,'$cgry',$qty,'$saler',$total_price,'$payment_type','$customer')";
							if($this->drink->saleitem($query)){
								$query="UPDATE drinks SET size=$new_stock WHERE item_id=$item_id";
									if($this->drink->update_item($query)){
									$sales++;}
								}
								else{
									redirect('counter');
								}

							}
							
							if ($sales>0) {
								$this->cart->destroy();
								$this->session->set_userdata('success','Successfull');
								redirect('counter');		
								}
							else{
								redirect('counter');
							}
										
							}
							else{
									redirect('counter');
								}
				}
				elseif ($salemode=="whole_sale") {
						if($this->cart->contents()){
						$sales=0;
						foreach ($this->cart->contents() as $item) {
							$name=$item['name'];
							$stock=$item['stock'];
							$item_id=(int)$item['id'];
							$qty=(int)$item['qty'];
							$cgry=$item['category'];
							$saler=$this->session->userdata('person_id');
							$sale_price=$item['price'];
							$total_price=($item['qty']*$sale_price);
							$new_stock=($stock-$qty);
							if ($new_stock<0) {
								$this->session->set_userdata('success','No Drugs Availabe');
								redirect('counter');
							}
							$new_stock=(int)$new_stock;
							$customer=$_POST['customer'];
							$time=date("Y-m-d",time());
							$query="INSERT INTO drinkSales VALUES('','$time','$name',$item_id,'$cgry',$qty,'$saler',$total_price,'whole_sale','$customer')";
							if($this->drink->saleitem($query)){
								$query="UPDATE drinks SET size=$new_stock WHERE item_id=$item_id";
									if($this->drink->update_item($query)){
									$sales++;}
								}
								else{
									redirect('counter');
								}

							}
							
							if ($sales>0) {
								$this->cart->destroy();
								$this->session->set_userdata('success','Successfull');
								redirect('counter');		
								}
							else{
								redirect('counter');
							}
										
							}
							else{
									redirect('counter');
								}
				}
			}
			else{
				$this->session->set_userdata('fail','Internal Error!');
				redirect('counter');
			}
		}
		else{
			redirect('counter');
		}
}

	function sale_mode(){
		$mode=$this->uri->segment(3);
		switch ($mode) {
			case 'drink':
				redirect('counter');
				break;
			case 'food':
				redirect('Sale_food');
				break;
			default:
				redirect('counter');
				break;
		}
	}

	function suspended(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$saler=(int)$this->session->userdata('person_id');
		$suspended=$this->drink->get_suspended_sales_drink($saler);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'counter/suspended_sales','title'=>'Other Sales','allowed_modules'=>$allowed_modules,'suspended'=>$suspended);
		$this->load->view('template',$data);

	}

	function search_suspended_sale(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['search_sale'])) {
			$search=$this->input->post('keyword');
			if (empty($search)) {
				redirect('counter/suspended');
			}
		$query="SELECT * FROM drinkSales WHERE name LIKE '%$search%' OR saler_id LIKE '%$search%' AND payment_type!='Cash'";
		$suspended=$this->drink->search_sale($query);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'counter/suspended_sales','title'=>'Other Sales','allowed_modules'=>$allowed_modules,'suspended'=>$suspended);
		$this->load->view('template',$data);

		}
		else{
			redirect('counter/suspended');
		}
	}


	function unsuspend(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$id=$this->uri->segment(3);
		$sale_time=date('Y-m-d',time());
		$query="UPDATE drinkSales SET payment_type='Cash',sale_time='$sale_time' WHERE sale_id='$id'";
		$suspended_sale_qry="SELECT * FROM drinkSales WHERE sale_id='$id'";
		$suspended_sale_r=$this->drink->suspended_particular($suspended_sale_qry);
		$suspended_sale_r=$suspended_sale_r->result();
		$drink_id=(int)$suspended_sale_r[0]->item_id;
		$amount=(int)$suspended_sale_r[0]->amount;
		if ($this->drink->remove_suspended($query)){
		$item_particula="SELECT size FROM drinks WHERE item_id=$drink_id";
		if($item_particula=$this->drink->suspenditem_particular($item_particula)){
			$item_particula=$item_particula->result();
			$stock=(int)$item_particula[0]->size;
			$new_stock=(int)$stock-$amount;
			$query="UPDATE drinks SET size='$new_stock' WHERE item_id=$drink_id";
			if($this->drink->update_original_item_size($query)){
				$this->session->set_userdata('success','Operation Successfull');
			redirect('counter/suspended');
			}
			else{
				$this->session->set_userdata('fail','Operation fail');
				redirect('counter/suspended');
			}}
		}}

		function remove_suspended_sale(){
			if(!$this->employee->is_logged_in()){
			redirect('login');
			}
			$id=$this->uri->segment(3);
			if ($this->drink->delete_suspended_drink($id)) {
				$this->session->set_userdata('success','Operation Successfull');
				redirect('counter/suspended');
			}
			else{
				$this->session->set_userdata('fail','Operation fail');
			redirect('counter/suspended');
			}
		}

function todaySales(){
			if(!$this->employee->is_logged_in()){
			redirect('login');
			}
		if (isset($_POST['todaySales'])) {
		$today=date("Y-m-d",time());
		$this->session->set_userdata('from',$today);
		$this->session->set_userdata('to',$today);
		$saler=(int)$this->session->userdata('person_id');
		$total_cash="SELECT SUM(price) AS value_sum FROM drinkSales WHERE payment_type='Cash' AND sale_time='$today' AND saler_id='$saler'";
		$item_sold="SELECT  DISTINCT item_id FROM drinkSales WHERE payment_type='Cash' AND sale_time='$today' AND saler_id='$saler'";
		$total_cash=$this->report->sales($total_cash);
		$sold_item_name=$this->report->sales($item_sold);
	$this->session->set_userdata('sold_item_names',$sold_item_name);
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='counter/todaySales';$data['title']="Summary sales";
	$data['cash']=$total_cash;
	$data['sold_item_name']=$sold_item_name;
	$this->load->vars($data);
	$this->load->view("template",$data);
		}
		else{
			redirect('counter');
		}
}
function allSales(){
			if(!$this->employee->is_logged_in()){
			redirect('login');
			}
	if (isset($_POST['allSales'])) {
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='report/all_drinks_sales';$data['title']="ALL SALES";
	$this->load->vars($data);
	$this->load->view("template",$data);
	}
	else{
			redirect('counter/allSales');
	}
}

	function updateSale(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['updateSale'])) {
			$this->form_validation->set_rules('drink_id','id','required');
			$this->form_validation->set_rules('sale_id','id','required');
			$this->form_validation->set_rules('new_sale_qty','qty','required');
			$this->form_validation->set_rules('customer','customer','required');
			$this->form_validation->set_rules('old_sale_qty','qty','required');
				if ($this->form_validation->run()==TRUE) {
				$item_id=(int)$this->input->post('drink_id');
				$sale_id=(int)$this->input->post('sale_id');
				$old_sale_qty=(int)$this->input->post('old_sale_qty');
				$new_sale_qty=(int)$this->input->post('new_sale_qty');
				$customer=$this->input->post('customer');
			if ($this->drink->update_sale($item_id,$new_sale_qty,$customer,$old_sale_qty,$sale_id)){
					$this->session->set_userdata('success','Operation Successfull');
					redirect('drinks_order');
				}
				else{
					$this->session->set_userdata('fail','Operation fail');
					redirect('drinks_order');
				}
			}
			else{
				redirect('drinks_order');
			}
		}
			elseif (isset($_POST['deleteSale'])) {
			$this->form_validation->set_rules('drink_id','id','required');
			$this->form_validation->set_rules('sale_id','id','required');
				if ($this->form_validation->run()==TRUE) {
				$item_id=(int)$this->input->post('drink_id');
				$sale_id=(int)$this->input->post('sale_id');
				if ($this->drink->delete_sale($sale_id,$item_id)){
					$this->session->set_userdata('success','Operation Successfull');
					redirect('drinks_order');
				}
				else{
					$this->session->set_userdata('fail','Operation fail');
					redirect('drinks_order');
				}
			}
			else{
				redirect('drinks_order');
			}
		}
		else{
			redirect('drinks_order');
		}
	}

	function saleOrder(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$order_id=(int)$this->uri->segment(3);
		if (empty($order_id)) {
			redirect('drinks_order');
		}
		$query="SELECT * FROM drink_order inner join drinks on drink_order.drink_id=drinks.item_id WHERE drink_order.order_id=$order_id";
		if ($item=$this->drink->get_order_particular($query)) {
			$name=$item[0]['name'];
			$stock=(int)$item[0]['size'];
					$item_id=(int)$item[0]['item_id'];
					$qty=(int)$item[0]['qty'];
					$cgry=$item[0]['category'];
					$saler=(int)$item[0]['saler_id'];
					$price=(double)$item[0]['price'];
					$new_stock=(int)$stock-$qty;
					$customer=$item[0]['customer'];
					$time=date("Y-m-d",time());
				//preventing stock to be negative
					if ($new_stock<0){
						$this->session->set_userdata('fail','Low stock');
						redirect('drinks_order');
					}
		$query="INSERT INTO drinkSales VALUES('','$time','$name',$item_id,'$cgry',$qty,$saler,$price,1,'$customer')";
			if($this->drink->saleitem($query)){
				$query1="UPDATE drinks SET size=$new_stock WHERE item_id=$item_id";
					if($this->drink->update_item($query1)){
						$query2="UPDATE drink_order SET status=1 WHERE order_id=$order_id";
					if($this->drink->update_item($query2)){
						$this->session->set_userdata('success','Operation Successfull');
						redirect('drinks_order');	
					}
					else{
						$this->session->set_userdata('fail','Operation fail');
						redirect('drinks_order');
					}
					}
					else{
						redirect('drinks_order');
					}
					
				}
		}
		}
		function creditSaleOrder(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$order_id=(int)$this->uri->segment(3);
		if (empty($order_id)) {
			redirect('drinks_order');
		}
		$query="SELECT * FROM drink_order inner join drinks on drink_order.drink_id=drinks.item_id WHERE drink_order.order_id=$order_id";
		if ($item=$this->drink->get_order_particular($query)) {
			$name=$item[0]['name'];
			$stock=(int)$item[0]['size'];
					$item_id=(int)$item[0]['item_id'];
					$qty=(int)$item[0]['qty'];
					$cgry=$item[0]['category'];
					$saler=(int)$item[0]['saler_id'];
					$price=(double)$item[0]['price'];
					$new_stock=(int)$stock-$qty;
					$customer=$item[0]['customer'];
					$time=date("Y-m-d",time());
				//preventing stock to be negative
					if ($new_stock<0){
						$this->session->set_userdata('fail','Low stock');
						redirect('drinks_order');
					}
		$query="INSERT INTO drinkSales VALUES('','$time','$name',$item_id,'$cgry',$qty,$saler,$price,0,'$customer')";
			if($this->drink->saleitem($query)){
				$query1="UPDATE drinks SET size=$new_stock WHERE item_id=$item_id";
					if($this->drink->update_item($query1)){
						$query2="UPDATE drink_order SET status=1 WHERE order_id=$order_id";
					if($this->drink->update_item($query2)){
						$this->session->set_userdata('success','Operation Successfull');
						redirect('drinks_order');	
					}
					else{
						$this->session->set_userdata('fail','Operation fail');
						redirect('drinks_order');
					}
					}
					else{
						redirect('drinks_order');
					}
					
				}
		}

	}

	 function receipt(){
	 		$this->load->library('cart');
	 		$this->load->library('pdf');
			$this->load->view('receipt');
			//$this->cart->destroy();
	 }
	 function removeOrder(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$order_id=(int)$this->uri->segment(3);
		if (empty($order_id)) {
			redirect('drinks_order');
		}
		$query="DELETE FROM drink_order WHERE order_id=$order_id";
			if ($this->drink->update_item($query)) {
				$this->session->set_userdata('success','Order Canceled');
				redirect('drinks_order');
			}
			else{
				$this->session->set_userdata('fail','Operation failed Please try Again');
				redirect('drinks_order');
			}			
						
	}

}//end of controller