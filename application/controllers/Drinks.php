<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Drinks extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('employee');	
		$this->load->model('module');
		$this->load->model('drink');
		$this->load->helper('function');
		$this->load->library('cart');
		$this->load->library('csvimport');
	}
	
	public function index(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$result=$this->drink->getall();
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'drinks/all','title'=>'DRUGS','allowed_modules'=>$allowed_modules,'item_result'=>$result);
		$this->load->view('template',$data);
	}
	public function save(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['save_item'])) {
			$this->load->library('form_validation');
			$this->load->helper('string');
			$this->form_validation->set_rules('name','drink Name','required');
			$this->form_validation->set_rules('category','drink category','required');
			$this->form_validation->set_rules('amount','drink Amount','required');
			$this->form_validation->set_rules('price','price','required');
			$this->form_validation->set_rules('cost','cost','required');
			$this->form_validation->set_rules('reorder_level','reorder_level','required');
			if ($this->form_validation->run()==TRUE) {
				$whole_sale_price=0;
				$name=$this->input->post('name');
				$barcode="None";
				$category=$this->input->post('category');
				$amount=$this->input->post('amount');
				$price=$this->input->post('price');
				$NHIF=(double)$this->input->post('NHIF');
				$TANESCO=(double)$this->input->post('TANESCO');
				$STRATEGY=(double)$this->input->post('STRATEGY');
				$BUNGE=(double)$this->input->post('BUNGE');
				$cost=$this->input->post('cost');
				$expire_date=$this->input->post('expire_date');
				$reorder_level=(int)$this->input->post('reorder_level');
				///////////////////////////////////////////////////////
				// $package=$this->input->post('package');
				// $package_amount=$this->input->post('package_amount');
				// $cost_per_unit_wholesale=$this->input->post('cost_per_unit_wholesale');
				// $whole_sale_price=$this->input->post('whole_sale_price');
				$package="None";
				$package_amount=0;
				$cost_per_unit_wholesale=0;
				$whole_sale_price=0;
				////////////////////////////////////////////////////
	$query="INSERT INTO drinks VALUES('$name','$category','$amount',' ',$price,$cost,'$barcode',$NHIF,$STRATEGY,$TANESCO,$BUNGE,'$expire_date',$reorder_level,'$package',$package_amount,$cost_per_unit_wholesale,$whole_sale_price)";
				if($this->drink->saveitem($query)){
					$this->session->set_userdata('success','Drug Added Successfull');
					redirect('drinks');
				}
			}
			if ($this->form_validation->run()==FALSE) {
				$this->session->set_userdata('fail','Fill All Required Fields PLease!');
				redirect('drinks');
			}
		}
		else {
			redirect('drinks');
		}
	}

	function generate_barcode($item_id)
	{
		$result = array();
		$item_id=(int)$item_id;
		$query="SELECT * FROM drinks WHERE item_id=$item_id";
		$item_info=$this->drink->search($query);
		
			
			$result[] = array('name' =>$item_info[0]->name, 'id'=> $item_id);
			$data['items'] = $result;
			$this->load->view("barcode_sheet", $data);
	}

	public function search(){
		$search=$this->uri->segment(3);
		if (isset($_POST['search_drink'])) {
			$search=$this->input->post('keyword');
		}
		$query="SELECT * FROM drinks WHERE name LIKE '$search%' OR item_id OR barcode LIKE '$search%'";
		$result=$this->drink->search($query);
		if (!$result) {
			redirect('drinks');
		}
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'drinks/all','title'=>'DRUGS','allowed_modules'=>$allowed_modules,'item_result'=>$result);
		$this->load->view('template',$data);

	}

	public function filter(){
		$no=$this->uri->segment(3);
		$result=$this->drink->filter($no);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'drinks/all','title'=>'DRUGS','allowed_modules'=>$allowed_modules,'item_result'=>$result);
		$this->load->view('template',$data);
	}

	function update(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['update_item'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('name','name','required');
			$this->form_validation->set_rules('category','category','required');
			$this->form_validation->set_rules('amount','amount','required');
			$this->form_validation->set_rules('item_id','id','required');
			$this->form_validation->set_rules('cash','price','required');
			$this->form_validation->set_rules('NHIF','price','required');
			$this->form_validation->set_rules('STRATEGY','price','required');
			$this->form_validation->set_rules('TANESCO','price','required');
			$this->form_validation->set_rules('BINGE','price','required');
			$this->form_validation->set_rules('reorder_level','reorder_level','required');
			$this->form_validation->set_rules('cost','cost','required');
			$this->form_validation->set_rules('expire_date','expire_date','required');
			if ($this->form_validation->run()==TRUE) {
				$reorder_level=(float)$this->input->post('reorder_level');
				$name=$this->input->post('name');
				$category=$this->input->post('category');
				$amount=(int)$this->input->post('amount');
				$cash=(double)$this->input->post('cash');
				$NHIF=(double)$this->input->post('NHIF');
				$STRATEGY=(double)$this->input->post('STRATEGY');
				$TANESCO=(double)$this->input->post('TANESCO');
				$BINGE=(double)$this->input->post('BINGE');
				$cost=(float)$this->input->post('cost');
				$id=(int)$this->input->post('item_id');
				$expire_date=$this->input->post('expire_date');
			$query="UPDATE drinks SET name='$name',category='$category',
			size=$amount,price=$cash,cost=$cost,NHIF=$NHIF,TANESCO=$TANESCO,STRATEGY=$STRATEGY,BINGE=$BINGE,reorder_level=$reorder_level,expire_date='$expire_date' WHERE item_id=$id";
				if($this->drink->update_item($query)){
				$this->session->set_userdata('success','Update is Successfull');
					redirect('drinks');
				}
				else{
				$this->session->set_userdata('fail','Fail! Item Is Up To Date');
				redirect('drinks');
				}
			}
			else{
				$this->session->set_userdata('fail','No Update Commited');
				redirect('drinks');
			}
		}
		elseif (isset($_POST['deleteSale'])) {
			//die();
			redirect('drinks');
		}
		else{
		$id=(int)$this->uri->segment(3);
		$query="SELECT * FROM drinks WHERE item_id=$id LIMIT 1";
		$result=$this->drink->get_item_particular($query);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'drinks/update','title'=>'DRINKS','allowed_modules'=>$allowed_modules,'row'=>$result);
		$this->load->view('template',$data);
		}
	}

	public function delete(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$id=$this->uri->segment(3);
		if($this->drink->remove_item($id)){
			$this->session->set_userdata('success','Operation Successfull');
			redirect('drinks');

		}
		redirect('drinks');	
	}


	function import(){
		if (isset($_POST['upload'])) {
				$file=$_FILES["drugs"]["tmp_name"];
				$file_data=$this->csvimport->get_array($file);
				foreach ($file_data as $row) {
					if (!isset($row["Drug Name"]) OR !isset($row["Form"]) OR !isset($row["Retail Price"]) OR !isset($row["Purchased"]) OR !isset($row["Package"]) OR !isset($row["Total Package"]) OR !isset($row["Cost_per_package"]) OR !isset($row["Whole_sale_price"])){
						$this->session->set_userdata('fail','Sorry! Incorrect File format');
						redirect('drinks');
					}
						$data[] = array(
							'drug_name'     => 		$row["Drug Name"],
							'formulation'   => 		$row["Form"],
							'stock'         =>  	$row["Stock"],
							'cash_price'    =>  	$row["Retail Price"],
							'cost_per_unit' => 		$row["Purchased"],
							'package'       =>  	$row["Package"],
							'package_amount'=>		$row["Total Package"],
							'cost_per_package'=>	$row["Cost_per_package"],
							'whole_sale_price'=>	$row["Whole_sale_price"]
						);
					}
					
					$this->drink->import_csv($data);
		}
		else{
					$this->session->set_userdata('fail','Error!! Select a Valid file');
					redirect('drinks');
		}

	}

}