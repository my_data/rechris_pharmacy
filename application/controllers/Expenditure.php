<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expenditure extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('employee');	
		$this->load->model('module');
		$this->load->model('accomodation_model');
		$this->load->model('exp');
		$this->load->model('drink');
	}

	function index(){
	if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$exp=$this->exp->getall();		
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'expenditure','title'=>'Expenditure','allowed_modules'=>$allowed_modules,'exp_result'=>$exp);
		$this->load->view('template',$data);

	}

	function save(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['save_exp'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('description','description','required');
			$this->form_validation->set_rules('cost','cost','required');
			$this->form_validation->set_rules('source','source','required');
			if ($this->form_validation->run()==TRUE) {
				$des=$this->input->post('description');
				$cost=(int)$this->input->post('cost');
				$source=$this->input->post('source');
				$day=date("Y-m-d",time());
				$emp=$this->session->userdata('person_id');
				$query="INSERT INTO expenditure VALUES('','$des',$cost,'$day','$emp','$source')";
				if($this->exp->saveData($query)){
						$this->session->set_userdata('success','Operation Successfull');
						redirect('expenditure');
					}
				else{
					$this->session->set_userdata('fail','Operation fail');
					redirect('expenditure');
				}
			}
		}
		else{
					redirect('expenditure');
				}
	}
function addcomplementary(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['addcomplementary'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('drink_id','name','required');
			$this->form_validation->set_rules('receiver','receiver','required');
			$this->form_validation->set_rules('quantity','cost','required');
			$saler_id=$this->session->userdata('person_id');
			if ($this->form_validation->run()==TRUE) {
				$drinkid=(int)$this->input->post('drink_id');
				$receiver=$this->input->post('receiver');
				$qty=$this->input->post('quantity');
				$day=date("Y-m-d",time());
				$saler_id=$this->session->userdata('person_id');
				//searching of drink old stock query
				$search_item="SELECT size FROM drinks WHERE item_id=$drinkid";
				$search_item=$this->drink->search($search_item);
				$old_stock=$search_item[0]->size;
				$new_stock=(int)$old_stock-$qty;
			$query_save="INSERT INTO complementary VALUES('',$drinkid,'$qty','$receiver','$saler_id','$day')";
				//save complementary
				if($this->exp->saveComplementary($query_save)){
					$query_update="UPDATE drinks SET size=$new_stock WHERE item_id='$drinkid'";
					//update stock
					if($this->drink->update_item($query_update)){
					$this->session->set_userdata('success','Operation Successfull');	
						redirect('expenditure');
					}
					}
				else{
					redirect('expenditure');
				}
			}
		}
		else{
					redirect('expenditure');
				}
	}
	 function addSpoil(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['addspoil'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('drink_id','id','required');
			$this->form_validation->set_rules('quantity','qty','required');
			if ($this->form_validation->run()==TRUE) {
				$drinkid=(int)$this->input->post('drink_id');
				$qty=(int)$this->input->post('quantity');
				$day=date("Y-m-d",time());
				$saler_id=(int)$this->session->userdata('person_id');
				//searching of drink old stock query
				$search_item="SELECT size FROM drinks WHERE item_id=$drinkid";
				$search_item=$this->drink->search($search_item);
				$old_stock=$search_item[0]->size;
				$new_stock=(int)$old_stock-$qty;
			$query_save="INSERT INTO spoil VALUES('',$drinkid,$qty,$saler_id,'$day')";
				//save spoil in the table
				if($this->exp->saveComplementary($query_save)){
					$query_update="UPDATE drinks SET size=$new_stock WHERE item_id='$drinkid'";
					//update to new drink tock 
					if($this->drink->update_item($query_update)){	
						$this->session->set_userdata('success','Operation Successfull');
						redirect('expenditure');
					}
					}
				else{
					redirect('expenditure');
				}
			}
		}
		else{
					redirect('expenditure');
				}
	}

	function addOther(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['addOther'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('drink_id','id','required');
			$this->form_validation->set_rules('location','Location','required');
			$this->form_validation->set_rules('quantity','cost','required');
			$saler_id=$this->session->userdata('person_id');
			if ($this->form_validation->run()==TRUE) {
				$drinkid=(int)$this->input->post('drink_id');
				$location=$this->input->post('location');
				$qty=$this->input->post('quantity');
				$day=date("Y-m-d",time());
				$saler_id=$this->session->userdata('person_id');
				//searching of drink old stock query
				$search_item="SELECT size FROM drinks WHERE item_id=$drinkid";
				$search_item=$this->drink->search($search_item);
				$old_stock=(int)$search_item[0]->size;
				$new_stock=(int)$old_stock-$qty;
			$query_save="INSERT INTO absorbed_drinks VALUES('',$drinkid,$qty,'$location',$saler_id,'$day')";
				//save complementary
				if($this->exp->saveComplementary($query_save)){
					$query_update="UPDATE drinks SET size=$new_stock WHERE item_id='$drinkid'";
					//update stock
					if($this->drink->update_item($query_update)){	
						$this->session->set_userdata('success','Operation Successfull');
						redirect('expenditure');
					}
					}
				else{
					redirect('expenditure');
				}
			}
		}
		else{
					redirect('expenditure');
				}
	}
	public function search(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$search=$this->uri->segment(3);
		if (isset($_POST['search_item'])) {
			$search=$this->input->post('keyword');
		}
		$query="SELECT * FROM expenditure WHERE des LIKE '%$search%' OR day LIKE '%$search%'";
		$exp=$this->exp->search_exp($query);
		if (!$exp) {
			redirect('expenditure');
		}
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'expenditure','title'=>'Expenditure','allowed_modules'=>$allowed_modules,'exp_result'=>$exp);
		$this->load->view('template',$data);
	}
	public function filter(){
		$no=$this->uri->segment(3);
		$exp=$this->exp->filter_exp($no);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'expenditure','title'=>'Expenditure','allowed_modules'=>$allowed_modules,'exp_result'=>$exp);
		$this->load->view('template',$data);
	}

	function update(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['update_exp'])) {
			$id=$this->uri->segment(3);
			$this->load->library('form_validation');
			$this->form_validation->set_rules('description','description','required');
			$this->form_validation->set_rules('cost','cost','required');
			$this->form_validation->set_rules('exp_id','id','required');
			if ($this->form_validation->run()==TRUE) {
				$des=$this->input->post('description');
				$id=$this->input->post('exp_id');
				$cost=(int)$this->input->post('cost');
				$query="UPDATE expenditure SET des='$des',cost=$cost WHERE exp_id='$id'";
				if($this->exp->update_exp($query)){
				$this->session->set_userdata('success','Operation Successfull');
				redirect('expenditure');						
					}
				else{
					$this->session->set_userdata('fail','Operation fail');
					redirect('expenditure');
				}
			}
		}
		else{
		$id=$this->uri->segment(3);
		$query="SELECT * FROM expenditure WHERE exp_id='$id' LIMIT 1";
		$exp=$this->exp->get_exp_particular($query);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'update_exp','title'=>'Update_exp','allowed_modules'=>$allowed_modules,'row'=>$exp);
		$this->load->view('template',$data);
		}
	}
	public function delet(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$id=$this->uri->segment(3);
		if($this->exp->remove_exp($id)){
			$this->session->set_userdata('success','Operation Successfull');
			redirect('expenditure');

		}	
	}
}
