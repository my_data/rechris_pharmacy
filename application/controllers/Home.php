<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller{
	function __construct()
	{

		parent::__construct();
		$this->load->model('employee');	
		$this->load->model('module');
		$this->load->model('drink');
		$this->load->model('food');	
		$this->load->model('accomodation_model');
		$this->load->model('report');		
	}
		
	public function index(){
		$data=array();
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='home';
	//$data['Report']=$this->drink->drinksReport();
	//$data['food_graph']=$this->food->foodGraph();
	//$data['room_graph']=$this->accomodation_model->roomGraph();
	$data['title']="Dashboard";	
	//$this->load->vars($data);
	//var_dump($data); die();
	$this->load->view("template",$data);
	}
	public function get_module(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$module_id=$this->uri->segment(3);
		$module=$this->module->getmodule($module_id);
		$module_name=$module->result();
		$modulename=$module_name[0]->name;
		redirect($modulename);
	}
	public function sales_per_month(){
		$saler=$this->session->userdata('person_id');
		$january_sales="SELECT sum(price) AS total_sales FROM drinksales  WHERE saler_id=$saler AND sale_time BETWEEN '2019-01-01' AND '2019-01-31'";
		$february_sales="SELECT sum(price) AS total_sales FROM drinksales WHERE saler_id=$saler AND sale_time BETWEEN '2019-02-01' AND '2019-02-28'";
		$march_sales="SELECT sum(price) AS total_sales FROM drinksales WHERE saler_id=$saler AND sale_time BETWEEN '2019-01-01' AND '2019-01-31'";
		$april_sales="SELECT sum(price) AS total_sales FROM drinksales WHERE saler_id=$saler AND sale_time BETWEEN '2019-01-01' AND '2019-01-31'";
		$may_sales="SELECT sum(price) AS total_sales FROM drinksales WHERE saler_id=$saler AND sale_time BETWEEN '2019-05-01' AND '2019-05-31'";
		$june_sales="SELECT sum(price) AS total_sales FROM drinksales WHERE saler_id=$saler AND sale_time BETWEEN '2019-06-01' AND '2019-06-31'";
		$july_sales="SELECT sum(price) AS total_sales FROM drinksales WHERE saler_id=$saler AND sale_time BETWEEN '2019-07-01' AND '2019-07-31'";
		$august_sales="SELECT sum(price) AS total_sales FROM drinksales WHERE saler_id=$saler AND sale_time BETWEEN '2019-08-01' AND '2019-08-31'";
		$september_sales="SELECT sum(price) AS total_sales FROM drinksales WHERE saler_id=$saler AND sale_time BETWEEN '2019-01-01' AND '2019-01-31'";
		$october_sales="SELECT sum(price) AS total_sales FROM drinksales WHERE saler_id=$saler AND sale_time BETWEEN '2019-10-01' AND '2019-10-31'";
		$november_sales="SELECT sum(price) AS total_sales FROM drinksales WHERE saler_id=$saler AND sale_time BETWEEN '2019-11-01' AND '2019-11-31'";
		$december_sales="SELECT sum(price) AS total_sales FROM drinksales WHERE saler_id=$saler AND sale_time BETWEEN '2019-12-01' AND '2019-12-31'";
			
					$january_sales=(int)$this->report->get_report($january_sales);
					$february_sales=(int)$this->report->get_report($february_sales);
					$march_sales=(int)$this->report->get_report($march_sales);
					$april_sales=(int)$this->report->get_report($april_sales);
					$may_sales=(int)$this->report->get_report($may_sales);
					$june_sales=(int)$this->report->get_report($june_sales);
					$july_sales=(int)$this->report->get_report($july_sales);
					$august_sales=(int)$this->report->get_report($august_sales);	
					$september_sales=(int)$this->report->get_report($september_sales);			
					$october_sales=(int)$this->report->get_report($october_sales);
					$november_sales=(int)$this->report->get_report($november_sales);
					$december_sales=(int)$this->report->get_report($december_sales);
					 /*Create array of monthly sales*/
					$sales_report_per_month=array(
						'jan'=>$january_sales,
						'feb'=>$february_sales,
						'mar'=>$march_sales,
						'ap'=>$april_sales,
						'may'=>$may_sales,
						'june'=>$june_sales,
						'jul'=>$july_sales,
						'aug'=>$august_sales,
						'sept'=>$september_sales,
						'oct'=>$october_sales,
						'nov'=>$november_sales,
						'dec'=>$december_sales
					);

					$sales_report_per_month_json=json_encode($sales_report_per_month);
					 echo $sales_report_per_month_json;
					
	}
	function logout()
	{

		$this->employee->logout();
	}
}
?>