<?php
 defined('BASEPATH') or exit('No direct script access allowed');
 /**
 * 
 */
 class Modules extends CI_Controller{

 	public function index(){
 		if ($this->session->userdata('schoolname')) {
 		$module_id=$this->uri->segment(2);
 		$this->load->model('m_school');
 		$query="SELECT * FROM modules WHERE module_id='$module_id' AND status=1";
 		$result=$this->m_school->getModule($query);

 		if ($result->num_rows()==1) {

 			$modulename=$result->result_array();
 			$modulename=$modulename[0]['classname'];
 			redirect($modulename);
 		}
 		}
 		else{
 			redirect('login');
 		}
 }

 }