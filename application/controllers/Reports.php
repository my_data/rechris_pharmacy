<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reports extends CI_Controller{
	function __construct()
	{

		parent::__construct();
		$this->load->model('employee');	
		$this->load->model('accomodation_model');	
		$this->load->model('module');
		$this->load->model('report');
		$this->load->model('drink');
		$this->load->model('food');
		$this->load->helper('function');
		$this->load->helper('food');	
		$this->load->library('form_validation');
	}
		
	function index()
	{
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}

	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='report/dashboard';
	$data['title']="Reports";	
	$this->load->vars($data);
	$this->load->view("template",$data);
	}
public function drinkSales(){
	if(!$this->employee->is_logged_in()){
			redirect('login');
		}
if (isset($_POST['show_summary_drinks'])) {
	if (empty($_POST['from'])  AND empty($_POST['today'])) {
		redirect('reports/drinkSales/summary');
	}
	$from=$this->input->post('from');
	$to=$this->input->post('to');
	$this->session->set_userdata('from',$from);
	$this->session->set_userdata('to',$to);
	$item_sold="SELECT DISTINCT item_id FROM drinkSales WHERE sale_time BETWEEN '$from' AND '$to'";
	$total_cash="SELECT SUM(price) AS value_sum FROM drinkSales WHERE sale_time BETWEEN '$from' AND '$to'";
	///selection of employee sales
	if (!empty($_POST['employee_id'])) {
			$employee_id=$_POST['employee_id'];
	$item_sold="SELECT DISTINCT item_id FROM drinkSales WHERE saler_id=$employee_id AND sale_time BETWEEN '$from' AND '$to'";
	$total_cash="SELECT SUM(price) AS value_sum FROM drinkSales WHERE saler_id=$employee_id AND sale_time BETWEEN '$from' AND '$to'";
	}//end
	$total_cash=$this->report->sales($total_cash);
	$sold_item_name=$this->report->sales($item_sold);
	$this->session->set_userdata('sold_item_names',$sold_item_name);
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='report/summary_drinksales';
	$data['title']="Summary sales";
	$data['cash']=$total_cash;
	$data['sold_item_name']=$sold_item_name;$this->load->vars($data);
	$this->load->view("template",$data);
}
elseif(isset($_POST['show_detailed_drinks'])){
		if (empty($_POST['from'])  AND empty($_POST['today'])) {
			redirect('reports/drinkSales/detailed');
		}
	$from=$this->input->post('from');
	$to=$this->input->post('to');
	$this->session->set_userdata('from',$from);
	$this->session->set_userdata('to',$to);
	$item_sold_qry="SELECT * FROM drinkSales INNER JOIN employees ON drinkSales.saler_id=employees.employee_id WHERE  sale_time BETWEEN '$from' AND '$to'";
	$total_cash_qry="SELECT SUM(price) AS value_sum FROM drinkSales WHERE sale_time BETWEEN '$from' AND '$to'";
	if (!empty($_POST['search'])) {
		$search=$_POST['search'];
		$this->checkif_exist($search);
		$item_sold_qry="SELECT * FROM drinkSales INNER JOIN employees ON drinkSales.saler_id=employees.employee_id WHERE sale_time BETWEEN '$from' AND '$to' AND payment_type LIKE '%$search%'";
		}
	if(!$sold_items=$this->report->sales($item_sold_qry)){
		$this->check_report_type();
	}
	$total_cash=$this->report->sales($total_cash_qry);
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='report/detailed_drinksales';
	$data['title']="Detailed Report sales";
	$data['cash']=$total_cash;
	$data['sold_items']=$sold_items;
	$this->load->vars($data);
	$this->load->view("template",$data);
}
else{
		$this->check_report_type();
}
}
function check_report_type(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
	$report_type=$this->uri->segment(2);
	$type=$this->uri->segment(3);
	if ($report_type=='drinkSales') {
	switch ($type) {
	case 'summary':
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='report/summary_drinksales';
	$data['title']="Summary sales";	
	$this->load->vars($data);
	$this->load->view("template",$data);
	break;
	case 'detailed':
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='report/detailed_drinksales';
	$data['title']="Detailed Report";	
	$this->load->vars($data);
	$this->load->view("template",$data);	
	break;
	default:
	redirect('reports');
	break;
	}
	}
	if ($report_type=='foodSales') {
	switch ($type) {
	case 'summary':
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='report/summary_foodksales';
	$data['title']="Summary sales";	
	$this->load->vars($data);
	$this->load->view("template",$data);
	break;
	case 'detailed':
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='report/detailed_foodsales';
	$data['title']="Detailed Report";	
	$this->load->vars($data);
	$this->load->view("template",$data);	
	break;
	default:
	$this->index();
	break;
	}
	}
}

function detailed_sale_report($from,$to,$search=0){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$items_sold="SELECT * FROM sales WHERE payment_type='Cash' AND sale_time BETWEEN '$from' AND '$to'";
		if($sold_items=$this->report->sales($items_sold)){
			return $sold_items;
		}
		else {
			return false;
		}
}
function checkif_exist($search){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$query="SELECT * FROM drinkSales WHERE saler_id LIKE '%$search%' OR name LIKE  '%$search%'";
		if ($r=$this->report->sales($query)) {
			if ($r->num_rows()<1) {
				redirect('reports/sales/detailed');
			}
			else{
				return false;
			}
		}
		else{
				return false;
			}
}

function get_expenditures(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['show_expenditures'])) {
			if (empty($_POST['from'])  AND empty($_POST['today'])) {
			redirect('reports/get_expenditures');
		}
		$from=$this->input->post('from');
		$to=$this->input->post('to');
		$this->session->set_userdata('from',$from);
		$this->session->set_userdata('to',$to);
		if($result=$this->report->get_expenditures()){
			$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/expenditure';$data['title']="Expenditures";
		$data['result']=$result;$this->load->vars($data);
		$this->load->view("template",$data);
		}
		else{
			redirect('reports');
		}
		}
	else{
		if($result=$this->report->get_expenditures()){
			$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/expenditure';$data['title']="Expenditures";
		$data['result']=$result;$this->load->vars($data);
		$this->load->view("template",$data);
		}
		else{
			redirect('reports');
		}
	}
}

function get_complementary(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['show_complementary'])) {
			if (empty($_POST['from'])  AND empty($_POST['today'])) {
			redirect('reports/get_complementary');
		}
		$from=$this->input->post('from');
		$to=$this->input->post('to');
		$this->session->set_userdata('from',$from);
		$this->session->set_userdata('to',$to);
		if($result=$this->report->get_complementary()){
			$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/complementary';$data['title']="Complementary";
		$data['result']=$result;$this->load->vars($data);
		$this->load->view("template",$data);
		}
		else{
			redirect('reports/get_complementary');
		}
		}
	else{
		if($result=$this->report->get_complementary()){
			$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/complementary';$data['title']="Complementaries";
		$data['result']=$result;$this->load->vars($data);
		$this->load->view("template",$data);
		}
		else{
			redirect('reports/get_complementary');
		}
	}
}

function get_spoil(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['show_spoil'])) {
			if (empty($_POST['from'])  AND empty($_POST['today'])) {
			redirect('reports/get_spoil');
		}
		$from=$this->input->post('from');
		$to=$this->input->post('to');
		$this->session->set_userdata('from',$from);
		$this->session->set_userdata('to',$to);
		if($result=$this->report->get_spoil()){
			$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/spoil';$data['title']="Spoil";
		$data['result']=$result;$this->load->vars($data);
		$this->load->view("template",$data);
		}
		else{
			redirect('reports/get_spoil');
		}
		}
	else{
		if($result=$this->report->get_spoil()){
			$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/spoil';$data['title']="Spoil";
		$data['result']=$result;$this->load->vars($data);
		$this->load->view("template",$data);
		}
		else{
			redirect('reports/get_spoil');
		}
	}
}
function get_customers(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['show_employees'])) {
			if (empty($_POST['from'])  AND empty($_POST['today'])) {
			redirect('reports/get_customers');
		}
		$from=$this->input->post('from');
		$to=$this->input->post('to');
		$this->session->set_userdata('from',$from);
		$this->session->set_userdata('to',$to);
		if (isset($_POST['today'])) {
		$this->session->set_userdata('from',date("Y-m-d",time()));
		$this->session->set_userdata('to',date("Y-m-d",time()));
		}
		$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/employees';$data['title']="Employees Report";
		$this->load->vars($data);
		$this->load->view("template",$data);
	}
		
	else{
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/employees';$data['title']="Employees Report";
		$this->load->vars($data);
		$this->load->view("template",$data);	
	}
	}

function suspendedDrinkSales(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['show_drinkSales'])) {
			if (empty($_POST['from'])  AND empty($_POST['today'])) {
			redirect('reports/get_customers');
		}
		$from=$this->input->post('from');
		$to=$this->input->post('to');
		$this->session->set_userdata('from',$from);
		$this->session->set_userdata('to',$to);
		
		$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/employees';$data['title']="Credit Sales";
		$this->load->vars($data);
		$this->load->view("template",$data);
	}
		
	else{
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/suspended_sales';$data['title']="Spoil";
		$this->load->vars($data);
		$this->load->view("template",$data);	
	}
	}
function unsuspend(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$id=$this->uri->segment(3);
		$sale_time=date('Y-m-d',time());
		$query="UPDATE drinkSales SET payment_type='Cash',sale_time='$sale_time' WHERE sale_id='$id'";
		$suspended_sale_qry="SELECT * FROM drinkSales WHERE sale_id='$id'";
		$suspended_sale_r=$this->drink->suspended_particular($suspended_sale_qry);
		$suspended_sale_r=$suspended_sale_r->result();
		$drink_id=(int)$suspended_sale_r[0]->item_id;
		$amount=(int)$suspended_sale_r[0]->amount;
		if ($this->drink->remove_suspended($query)){
		$item_particula="SELECT size FROM drinks WHERE item_id=$drink_id";
		if($item_particula=$this->drink->suspenditem_particular($item_particula)){
			$item_particula=$item_particula->result();
			$stock=(int)$item_particula[0]->size;
			$new_stock=(int)$stock-$amount;
			$query="UPDATE drinks SET size='$new_stock' WHERE item_id=$drink_id";
			if($this->drink->update_original_item_size($query)){
			redirect('reports/suspendedDrinkSales');
			}
			else{
				redirect('reports/suspendedDrinkSales');
			}}
		}}

	function get_receiving(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
			$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/receiving';$data['title']="Receiving";
		$this->load->vars($data);
		$this->load->view("template",$data);
	}

function get_drinks_stock(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['show_stocking'])) {
			if (empty($_POST['from']) && empty($_POST['to'])) {
			$this->session->set_userdata('fail','Please Select Time Range');
			redirect('reports/get_drinks_stock');
		}
		$this->session->set_userdata('from',$_POST['from']);
		$this->session->set_userdata('to',$_POST['to']);
		if($result=$this->report->drinks_stocking()){
		$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/drinks_stocking';$data['title']="Drugs Stocking";
		$data['result']=$result;$this->load->vars($data);
		$this->load->view("template",$data);
		}
		else{
			redirect('reports/get_drinks_stock');
		}
		}
	else{
		
		$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/drinks_stocking';$data['title']="Drinks Stocking";
		$this->load->vars($data);
		$this->load->view("template",$data);
	}
	}

function nhif(){
	if(isset($_POST['nhif_sales'])){
		if (empty($_POST['from'])  AND empty($_POST['today'])) {
			redirect('reports/drinkSales/detailed');
		}
	$from=$this->input->post('from');
	$to=$this->input->post('to');
	$this->session->set_userdata('from',$from);
	$this->session->set_userdata('to',$to);
	$item_sold_qry="SELECT * FROM drinkSales INNER JOIN employees ON drinkSales.saler_id=employees.employee_id WHERE  sale_time BETWEEN '$from' AND '$to'";
	$total_cash_qry="SELECT SUM(price) AS value_sum FROM drinkSales WHERE sale_time BETWEEN '$from' AND '$to'";
	if (!empty($_POST['search'])) {
		$search=$_POST['search'];
		$this->checkif_exist($search);
		$item_sold_qry="SELECT * FROM drinkSales INNER JOIN employees ON drinkSales.saler_id=employees.employee_id WHERE sale_time BETWEEN '$from' AND '$to' AND saler_id LIKE '%$search%' OR name LIKE  '%$search%'";
		}
	if(!$sold_items=$this->report->sales($item_sold_qry)){
		$this->check_report_type();
	}
	$total_cash=$this->report->sales($total_cash_qry);
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='report/detailed_drinksales';
	$data['title']="Detailed Report sales";
	$data['cash']=$total_cash;
	$data['sold_items']=$sold_items;
	$this->load->vars($data);
	$this->load->view("template",$data);
}
else{
	$total_cash=0;
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='report/nhif';
	$data['title']="NHIF sales";
	$data['cash']=0;
	$data['sold_items']=0;
	$this->load->vars($data);
	$this->load->view("template",$data);
}
}
	
}//end of controller
