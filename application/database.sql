-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 08, 2019 at 12:15 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rechris`
--

-- --------------------------------------------------------

--
-- Table structure for table `absorbed_drinks`
--

DROP TABLE IF EXISTS `absorbed_drinks`;
CREATE TABLE IF NOT EXISTS `absorbed_drinks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `drink_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `location` varchar(50) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `day` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `drink_id` (`drink_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absorbed_drinks`
--

INSERT INTO `absorbed_drinks` (`id`, `drink_id`, `qty`, `location`, `emp_id`, `day`) VALUES
(2, 5, 5, 'conference', 1, '2018-05-17'),
(3, 15, 5, 'conference', 1, '2018-07-20');

-- --------------------------------------------------------

--
-- Table structure for table `complementary`
--

DROP TABLE IF EXISTS `complementary`;
CREATE TABLE IF NOT EXISTS `complementary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `drink_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `receiver` varchar(50) NOT NULL,
  `saler_id` int(11) NOT NULL,
  `day` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `complementary`
--

INSERT INTO `complementary` (`id`, `drink_id`, `quantity`, `receiver`, `saler_id`, `day`) VALUES
(1, 1006, 45, 'ima', 1, '2018-12-11');

-- --------------------------------------------------------

--
-- Table structure for table `drinks`
--

DROP TABLE IF EXISTS `drinks`;
CREATE TABLE IF NOT EXISTS `drinks` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` double NOT NULL,
  `item_id` int(10) NOT NULL AUTO_INCREMENT,
  `price` double NOT NULL,
  `cost` float NOT NULL,
  `barcode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NHIF` double DEFAULT NULL,
  `STRATEGY` double DEFAULT NULL,
  `TANESCO` double DEFAULT NULL,
  `BINGE` double DEFAULT NULL,
  `expire_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reorder_level` float DEFAULT NULL,
  `package` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `package_amount` int(11) DEFAULT NULL,
  `cost_per_package` float DEFAULT NULL,
  `whole_sale_price` float DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1028 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `drinks`
--

INSERT INTO `drinks` (`name`, `category`, `size`, `item_id`, `price`, `cost`, `barcode`, `NHIF`, `STRATEGY`, `TANESCO`, `BINGE`, `expire_date`, `reorder_level`, `package`, `package_amount`, `cost_per_package`, `whole_sale_price`) VALUES
('Hedex', 'Tablet', 4473, 1006, 300, 45, '', 150, 300, 200, 300, '2019-04-14', NULL, '', 0, 0, 0),
('Amoxilin', 'Tablet', 4985, 1007, 200, 10, 'None', 200, 400, 200, 300, '2019-04-14', NULL, '', 0, 0, 0),
('Albendazole', 'Tablet', 735, 1008, 300, 20, 'None', 200, 400, 300, 300, '2019-10-04', 700, '', 0, 0, 0),
('Acetazolamide(250mg)', 'Tablet', 344, 1009, 459, 45, 'None', 567, 543, 234, 678, '2020-01-01', 3380, '', 0, 0, 0),
('Ampoxicillin (500mg)', 'Tablet', 5096, 1010, 500, 50, 'None', 300, 400, 400, 509, '2019-04-14', NULL, '', 0, 0, 0),
('Magnezium', 'Pessaries', 3000, 1011, 300, 20, 'None', 500, 789, 309, 450, '2019-04-14', 50, '', 0, 0, 0),
('Amoxnillon', 'Injection', 4997, 1012, 500, 400, 'None', 700, 890, 1000, 800, '2019-04-14', 5000, '', 0, 0, 0),
('Amoxnillonida', 'Capsules', 5972, 1013, 300, 20, 'None', 509, 456, 509, 567, '2019-04-14', 300, '', 0, 0, 0),
('Diclopar', 'Gel', 5897, 1014, 400, 300, 'None', 590, 500, 340, 423, '2019-04-14', 200, '', 0, 0, 0),
('Panadol', 'Tablet', 4500, 1015, 789, 67, 'None', 456, 654, 456, 423, '2019-02-15', 58, '', 0, 0, 0),
('Amoxnillonida', 'Tablet', 34, 1016, 34, 45, 'None', 54, 45, 564, 67, '2019-02-08', 34, '', 0, 0, 0),
('Mucolyn', 'Syrup', 40, 1017, 1, 1000, 'None', 1, 1, 1, 1, '2019-09-27', 50, 'Box', 50, 50000, 60000),
('Chlorophenical', 'Tablet', 50, 1018, 450, 320, 'None', 1, 1, 1, 1, '2019-09-05', 50, 'Box', 100, 2000, 2500),
('Benzene', 'Drops', 897, 1019, 500, 100, 'None', 0, 0, 0, 0, '2020-01-02', 60, 'Tin', 100, 1000, 2000),
('ARV', 'Tablet', 5000, 1020, 100, 50, '0', 0, 0, 0, 0, '2020-01-01', 0, 'Box', 50, 500, 6000),
('Nairobi1', 'Tablet', 5000, 1021, 100, 50, '0', 0, 0, 0, 0, '2019-04-16', 0, 'Box', 50, 500, 6000),
('Nairobi2', 'Tablet2', 5000, 1022, 100, 50, '0', 0, 0, 0, 0, '2019-04-16', 0, 'Box', 50, 500, 7000),
('Nairobi3', 'Tablet', 5000, 1023, 100, 50, '0', 0, 0, 0, 0, '2019-04-16', 0, 'Box', 50, 500, 8000),
('Nairobi4', 'Tablet', 5000, 1024, 100, 50, '0', 0, 0, 0, 0, '2020-02-08', 50, 'Box', 50, 500, 9000),
('Nairobi2', 'Tablet2', 5000, 1025, 100, 50, '0', 0, 0, 0, 0, '2019-04-16', 0, 'Box', 50, 500, 7000),
('Nairobi3', 'Tablet', 5000, 1026, 100, 50, '0', 0, 0, 0, 0, '2019-04-16', 0, 'Box', 50, 500, 8000),
('Nairobi4', 'Tablet', 5000, 1027, 100, 50, '0', 0, 0, 0, 0, '2019-04-16', 0, 'Box', 50, 500, 9000);

-- --------------------------------------------------------

--
-- Table structure for table `drinksales`
--

DROP TABLE IF EXISTS `drinksales`;
CREATE TABLE IF NOT EXISTS `drinksales` (
  `sale_id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_time` date NOT NULL,
  `name` varchar(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `category` varchar(20) NOT NULL,
  `amount` int(20) NOT NULL,
  `saler_id` varchar(20) NOT NULL,
  `price` double NOT NULL,
  `payment_type` varchar(50) NOT NULL,
  `customer` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`sale_id`),
  KEY `item_id` (`item_id`) USING BTREE,
  KEY `saler_id` (`saler_id`),
  KEY `sale_time` (`sale_time`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drinksales`
--

INSERT INTO `drinksales` (`sale_id`, `sale_time`, `name`, `item_id`, `category`, `amount`, `saler_id`, `price`, `payment_type`, `customer`) VALUES
(4, '2018-12-12', 'Hedex', 1006, 'Tablet', 11, '1', 3300, 'NHIF', 'shakiru'),
(6, '2018-12-19', 'Hedex', 1006, 'Tablet', 2, '1', 600, 'Cash', 'George Mwamasika'),
(7, '2018-12-14', 'Hedex', 1006, 'Tablet', 3, '1', 900, 'Cash', 'George'),
(9, '2019-01-16', 'Amoxilin', 1007, 'Tablet', 2, '1', 400, 'Cash', 'Abra'),
(10, '2019-01-17', 'Acetazolamide-250mg-', 1009, 'Tablet', 1, '1', 459, 'Cash', 'Abra'),
(11, '2019-01-17', 'Acetazolamide-250mg-', 1009, 'Tablet', 2, '1', 918, 'NHIF', 'Cosmas'),
(12, '2019-01-24', 'Ampoxicillin -500mg-', 1010, 'Tablet', 2, '1', 1000, 'Cash', 'Alan'),
(13, '2019-01-24', 'Acetazolamide-250mg-', 1009, 'Tablet', 2, '1', 680, 'Cash', 'Alan'),
(14, '2019-02-07', 'Acetazolamide-250mg-', 1009, 'Tablet', 3, '1', 1377, 'Cash', 'Abra'),
(15, '2019-02-07', 'Acetazolamide-250mg-', 1009, 'Tablet', 5, '1', 2835, 'NHIF', 'Alan'),
(18, '2019-02-07', 'Acetazolamide-250mg-', 1009, 'Tablet', 89, '1', 30260, 'Cash', 'Mbarouk'),
(20, '2019-02-07', 'Acetazolamide-250mg-', 1009, 'Tablet', 34, '1', 11560, 'Cash', 'Abdala'),
(22, '2019-02-07', 'Amoxilin', 1007, 'Tablet', 8, '1', 1600, 'NHIF', 'Aline'),
(23, '2019-02-07', 'Amoxnillonida', 1013, 'Capsules', 8, '1', 2400, 'BUNGE', 'Calvin'),
(25, '2019-02-07', 'Albendazole', 1008, 'Tablet', 5, '1', 1500, 'STRATEGY', 'Emma'),
(26, '2019-02-07', 'Albendazole', 1008, 'Tablet', 5, '1', 1500, 'TANESCO', 'Aisha'),
(27, '2019-02-07', 'Acetazolamide-250mg-', 1009, 'Tablet', 8, '1', 2720, 'STRATEGY', 'Linnah'),
(28, '2019-03-26', 'Diclopar', 1014, 'Gel', 2, '1', 800, 'NHIF', 'Joyce'),
(30, '2019-03-27', 'Mucolyn', 1017, 'Box', 1, '1', 1, 'whole_sale', 'Siphael'),
(31, '2019-03-27', 'Amoxnillon', 1012, 'Injection', 1, '1', 500, 'Cash', 'Tilya'),
(33, '2019-03-27', 'Chlorophenical', 1018, 'Box', 50, '1', 125000, 'whole_sale', 'Geg'),
(34, '2019-03-27', 'Benzene', 1019, 'Drops', 100, '1', 50000, 'Cash', 'August'),
(35, '2019-04-13', 'Acetazolamide-250mg-', 1009, 'Tablet', 2, '1', 918, 'Cash', 'Denis'),
(36, '2019-04-13', 'Albendazole', 1008, 'Tablet', 5, '1', 1500, 'Cash', 'Oswald'),
(37, '2019-04-13', 'Acetazolamide-250mg-', 1009, 'Tablet', 2, '1', 1000, 'Cash', 'Mustafer'),
(38, '2019-04-13', 'Acetazolamide-250mg-', 1009, 'Tablet', 2, '1', 1000, 'Cash', 'Mustafer'),
(39, '2019-04-13', 'Acetazolamide-250mg-', 1009, 'Tablet', 2, '1', 1000, 'Cash', 'Mustafer'),
(40, '2019-04-13', 'Acetazolamide-250mg-', 1009, 'Tablet', 2, '1', 1000, 'Cash', 'Mustafer'),
(42, '2019-04-13', 'Benzene', 1019, 'Drops', 3, '1', 1500, 'Cash', 'Jery'),
(43, '2019-04-13', 'Diclopar', 1014, 'Gel', 1, '1', 400, 'Cash', 'Jamaika'),
(44, '2019-04-13', 'Amoxnillon', 1012, 'Injection', 2, '1', 1000, 'Cash', 'Jamaika'),
(45, '2019-06-08', 'Amoxilin', 1007, 'Tablet', 5, '1', 1000, 'Cash', '');

-- --------------------------------------------------------

--
-- Table structure for table `drink_order`
--

DROP TABLE IF EXISTS `drink_order`;
CREATE TABLE IF NOT EXISTS `drink_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `drink_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double NOT NULL,
  `saler_id` int(11) NOT NULL,
  `customer` varchar(50) NOT NULL,
  `day` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `saler_id` (`saler_id`),
  KEY `drink_id` (`drink_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `drink_receives`
--

DROP TABLE IF EXISTS `drink_receives`;
CREATE TABLE IF NOT EXISTS `drink_receives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `drink_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `day` varchar(50) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `order_cost` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`),
  KEY `drink_id` (`drink_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`employee_id`),
  UNIQUE KEY `username` (`username`),
  KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`firstname`, `lastname`, `username`, `password`, `employee_id`) VALUES
('Abraham', 'Macha', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1),
('yuyhff', 'wert', 'php', 'cba2e39793995b3b682da78f854786f2', 16);

-- --------------------------------------------------------

--
-- Table structure for table `expenditure`
--

DROP TABLE IF EXISTS `expenditure`;
CREATE TABLE IF NOT EXISTS `expenditure` (
  `exp_id` int(11) NOT NULL AUTO_INCREMENT,
  `des` varchar(500) NOT NULL,
  `cost` float NOT NULL,
  `day` varchar(50) NOT NULL,
  `emp_id` varchar(11) NOT NULL,
  `source` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`exp_id`),
  KEY `emp_id` (`emp_id`),
  KEY `emp_id_2` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

DROP TABLE IF EXISTS `food`;
CREATE TABLE IF NOT EXISTS `food` (
  `food_id` int(11) NOT NULL AUTO_INCREMENT,
  `foodname` varchar(500) NOT NULL,
  `category` varchar(500) NOT NULL,
  `price` float NOT NULL,
  PRIMARY KEY (`food_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`food_id`, `foodname`, `category`, `price`) VALUES
(3, 'Bagia', 'Bites', 300),
(5, 'Wali Kuku', 'msos', 15000);

-- --------------------------------------------------------

--
-- Table structure for table `foodsales`
--

DROP TABLE IF EXISTS `foodsales`;
CREATE TABLE IF NOT EXISTS `foodsales` (
  `sale_id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_time` varchar(50) NOT NULL,
  `name` varchar(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `category` varchar(20) NOT NULL,
  `amount` int(20) NOT NULL,
  `saler_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `status` int(11) NOT NULL,
  `customer` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`sale_id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `foodsales`
--

INSERT INTO `foodsales` (`sale_id`, `sale_time`, `name`, `item_id`, `category`, `amount`, `saler_id`, `price`, `status`, `customer`) VALUES
(1, '2018-09-05', 'Bagia', 3, 'Bites', 1, 1, 300, 1, 'joy'),
(2, '2018-09-05', 'Wali Kuku', 5, 'msos', 2, 1, 30000, 1, 'Suzi'),
(3, '2018-09-08', 'Wali Kuku', 5, 'msos', 2, 1, 30000, 0, 'Osca'),
(4, '2018-09-08', 'Wali Kuku', 5, 'msos', 1, 1, 15000, 2, 'Vicky');

-- --------------------------------------------------------

--
-- Table structure for table `food_order`
--

DROP TABLE IF EXISTS `food_order`;
CREATE TABLE IF NOT EXISTS `food_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `food_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double NOT NULL,
  `saler_id` int(11) NOT NULL,
  `customer` varchar(50) NOT NULL,
  `day` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `food_id` (`food_id`),
  KEY `saler_id` (`saler_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `food_order`
--

INSERT INTO `food_order` (`order_id`, `food_id`, `qty`, `price`, `saler_id`, `customer`, `day`, `status`) VALUES
(1, 3, 5, 1500, 1, 'Alfa', '2018-05-20', 1),
(2, 5, 1, 15000, 1, 'ema', '2018-07-02', 1),
(3, 5, 1, 15000, 1, 'b', '2018-08-27', 0);

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

DROP TABLE IF EXISTS `income`;
CREATE TABLE IF NOT EXISTS `income` (
  `income_id` int(11) NOT NULL AUTO_INCREMENT,
  `des` varchar(100) NOT NULL,
  `fund` int(11) NOT NULL,
  `day` varchar(50) NOT NULL,
  `emp_id` varchar(50) NOT NULL,
  `source` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`income_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `income`
--

INSERT INTO `income` (`income_id`, `des`, `fund`, `day`, `emp_id`, `source`) VALUES
(7, 'Tigo', 500000, '2018-07-26', '1', 'CONFERENCE P'),
(6, 'voda', 500000, '2018-07-26', '1', 'SWIMMING POOL'),
(8, 'Tigo', 600000, '2018-07-26', '1', 'BOARD ROOM');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
CREATE TABLE IF NOT EXISTS `modules` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `classname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`module_id`),
  KEY `module_id` (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`module_id`, `name`, `status`, `classname`) VALUES
(1, 'Drinks ', 1, 'Store'),
(3, 'Counter', 1, 'Despensor'),
(4, 'Staff', 1, 'Staffs'),
(5, 'Receiving', 1, 'Receive'),
(8, 'Reports', 1, 'Reports'),
(9, 'Expenditure', 1, 'Spoil'),
(12, 'Drinks_order', 1, 'All Sales');

-- --------------------------------------------------------

--
-- Table structure for table `payment_method`
--

DROP TABLE IF EXISTS `payment_method`;
CREATE TABLE IF NOT EXISTS `payment_method` (
  `pyment_method_id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_name` varchar(50) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`pyment_method_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_method`
--

INSERT INTO `payment_method` (`pyment_method_id`, `payment_name`, `price`) VALUES
(1, 'Cash', 1),
(2, 'Strategy', 1),
(3, 'Tanesco', 1),
(4, 'Binge', 1);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  KEY `module_id` (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `module_id`, `employee_id`) VALUES
(81, 12, 14),
(82, 5, 14),
(83, 3, 14),
(99, 4, 16),
(100, 3, 16),
(101, 1, 16),
(102, 12, 1),
(103, 9, 1),
(104, 8, 1),
(105, 5, 1),
(106, 4, 1),
(107, 3, 1),
(108, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
CREATE TABLE IF NOT EXISTS `rooms` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `category` varchar(50) NOT NULL,
  `fee` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`room_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`room_id`, `name`, `category`, `fee`, `status`) VALUES
(6, 'GT 200', 'Executive Deluxe', '5000', 'EMPTY'),
(8, 'AB 100', 'Deluxe', '500000', 'EMPTY'),
(5, 'G20', 'Deluxe', '30000', 'FULL'),
(10, 'TX 100', 'Suite', '70000', 'EMPTY'),
(11, 'Anex', 'Executive Deluxe', '500000', 'EMPTY');

-- --------------------------------------------------------

--
-- Table structure for table `room_booking`
--

DROP TABLE IF EXISTS `room_booking`;
CREATE TABLE IF NOT EXISTS `room_booking` (
  `tras_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `saler_id` int(11) NOT NULL,
  `customer` varchar(50) NOT NULL,
  `price` double NOT NULL,
  `day` varchar(50) NOT NULL,
  `booked_day` varchar(50) NOT NULL,
  PRIMARY KEY (`tras_id`),
  KEY `saler_id` (`saler_id`),
  KEY `room_id` (`room_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_booking`
--

INSERT INTO `room_booking` (`tras_id`, `room_id`, `saler_id`, `customer`, `price`, `day`, `booked_day`) VALUES
(3, 2, 1, 'b', 20000, '2018-07-11', '2018-07-10'),
(4, 7, 1, 'lonac', 73747, '2018-07-11', '2018-07-10'),
(5, 4, 1, 'xsxsx', 1000, '2018-07-11', '2018-07-10');

-- --------------------------------------------------------

--
-- Table structure for table `soldroom`
--

DROP TABLE IF EXISTS `soldroom`;
CREATE TABLE IF NOT EXISTS `soldroom` (
  `roomsale_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_ID` int(11) NOT NULL,
  `roomsaler_id` int(11) NOT NULL,
  `roomcustomer` varchar(50) NOT NULL,
  `price` double NOT NULL,
  `day` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `checkout_time` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`roomsale_id`),
  KEY `room_ID` (`room_ID`),
  KEY `roomsaler_id` (`roomsaler_id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soldroom`
--

INSERT INTO `soldroom` (`roomsale_id`, `room_ID`, `roomsaler_id`, `roomcustomer`, `price`, `day`, `status`, `checkout_time`) VALUES
(1, 6, 1, 'abu', 5000, '2018-06-27', 0, NULL),
(2, 2, 1, 'abu', 20000, '2018-06-27', 0, NULL),
(3, 6, 1, 'abu', 5000, '2018-06-27', 0, NULL),
(4, 2, 1, 'ema', 20000, '2018-07-01', 0, NULL),
(5, 2, 1, 'macha', 20000, '2018-07-02', 0, NULL),
(6, 6, 1, 'joy', 5000, '2018-07-02', 0, NULL),
(7, 6, 1, 'macha', 5000, '2018-07-04', 0, NULL),
(8, 6, 1, 'joy', 5000, '2018-07-07', 0, NULL),
(9, 6, 1, 'lonac', 5000, '2018-07-07', 0, NULL),
(10, 6, 1, 'lonac', 5000, '2018-07-10', 0, ''),
(11, 5, 1, 'baraka', 30000, '2018-07-07', 0, '2018-07-09'),
(12, 6, 1, 'edccd', 5000, '2018-07-10', 0, '2018-07-10'),
(13, 4, 1, 'xcx', 1000, '2018-07-10', 0, ''),
(14, 6, 1, 'dwsds', 5000, '2018-07-10', 0, ''),
(15, 4, 1, 'zdsdsd', 1000, '2018-07-10', 0, '2018-07-10'),
(16, 5, 1, 'b n ', 30000, '2018-07-10', 0, ''),
(17, 4, 1, 'dfdf', 1000, '2018-07-10', 0, ''),
(18, 4, 1, 'dcvdfv', 1000, '2018-07-10', 0, '2018-07-10'),
(19, 6, 1, 'zxca', 5000, '2018-07-10', 0, ''),
(20, 6, 1, 'joy', 5000, '2018-08-30', 0, '2018-08-30'),
(21, 6, 1, 'lonac', 5000, '2018-08-30', 0, ''),
(22, 5, 1, 'abu', 30000, '2018-08-30', 2, ''),
(23, 10, 1, 'macha', 70000, '2018-09-02', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `spoil`
--

DROP TABLE IF EXISTS `spoil`;
CREATE TABLE IF NOT EXISTS `spoil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `saler_id` int(11) NOT NULL,
  `day` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  KEY `saler_id` (`saler_id`),
  KEY `item_id_2` (`item_id`),
  KEY `saler_id_2` (`saler_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spoil`
--

INSERT INTO `spoil` (`id`, `item_id`, `qty`, `saler_id`, `day`) VALUES
(1, 1005, 45, 1, '2018-12-11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `drinks`
--
ALTER TABLE `drinks` ADD FULLTEXT KEY `name` (`name`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `drinksales`
--
ALTER TABLE `drinksales`
  ADD CONSTRAINT `item_join_sales` FOREIGN KEY (`item_id`) REFERENCES `drinks` (`item_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `drink_order`
--
ALTER TABLE `drink_order`
  ADD CONSTRAINT `ioppytr` FOREIGN KEY (`saler_id`) REFERENCES `employees` (`employee_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `drink_receives`
--
ALTER TABLE `drink_receives`
  ADD CONSTRAINT `bnjuio` FOREIGN KEY (`emp_id`) REFERENCES `employees` (`employee_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `drcvyh` FOREIGN KEY (`drink_id`) REFERENCES `drinks` (`item_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
