<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 function get_sold_food_particular($id,$from,$to){
 	$id=(int)$id;
 	$ci=& get_instance();
 	$query="SELECT SUM(foodSales.amount) AS qty,SUM(foodSales.price) AS cash,food.foodname,food.category,food.price AS sale_price FROM foodSales inner join food on food.food_id=foodSales.item_id WHERE item_id=$id AND sale_time BETWEEN '$from' AND '$to'";
	$result=$ci->db->query($query);
	return $result->result();
 }

function get_sold_foodDetailed_particular($id,$from,$to){
 	$foodid=(int)$id;
  	$ci=& get_instance();
 	$query="SELECT foodSales.amount AS qty,foodSales.price AS cash,food.foodname,food.category,employees.firstname,employees.lastname,foodSales.sale_time,foodSales.customer FROM foodSales inner join food on food.food_id=foodSales.item_id inner join employees on employees.employee_id=foodSales.saler_id WHERE foodSales.item_id=$foodid AND foodSales.status=1 AND foodSales.sale_time BETWEEN '$from' AND '$to'";
	$result=$ci->db->query($query);
	return $result->result();
 }

function credit_per_food($id,$from,$to){
 	$foodid=(int)$id;
  	$ci=& get_instance();
 	$query="SELECT SUM(foodSales.price) AS credit FROM foodSales WHERE status=0 AND item_id=$id AND sale_time BETWEEN '$from' AND '$to'";
	$result=$ci->db->query($query);
	if ($result->num_rows()>0) {
		$res=$result->result();
		return $res[0]->credit;
	}
	else{
		return 0;
	}

 }
function complementary($id,$from,$to){
 	$foodid=(int)$id;
  	$ci=& get_instance();
 	$query="SELECT SUM(foodSales.price) AS complement FROM foodSales WHERE status=2 AND item_id=$id AND sale_time BETWEEN '$from' AND '$to'";
	$result=$ci->db->query($query);
	if ($result->num_rows()>0) {
		$res=$result->result();
		return $res[0]->complement;
	}
	else{
		return 0;
	}

 }
 
function get_employee_foodname($names_obj){
 	$ci=& get_instance();
 	$result=array();
 	foreach ($names_obj->result() as $name) {
 		$query="SELECT * FROM drinkSales INNER JOIN employees ON  employees.employee_id=drinkSales.saler_id WHERE drinkSales.saler_id='$name->saler_id'";
	    $result=$ci->db->query($query);
 	}
 	
	return $result;
 }

function count_customer_foosales($saler){
	$ci=& get_instance();
 	$from=$ci->session->userdata('from');
 	$to=$ci->session->userdata('to');
 	$query="SELECT employees.firstname,employees.lastname,SUM(drinkSales.price) AS total_sales FROM drinkSales inner join employees on saler_id=employee_id WHERE sale_time BETWEEN '$from' AND '$to'AND saler_id=$saler";
	$result=$ci->db->query($query);
	$r=$result->result();
	return $r;
}

function get_total_suspended_foodsales(){
 	$ci=& get_instance();
 	$from=$ci->session->userdata('from');
 	$to=$ci->session->userdata('to');
 	$query="SELECT * FROM foodSales WHERE status=0 AND sale_time BETWEEN '$from' AND '$to'";
	$result=$ci->db->query($query);
	return $result->num_rows();
 }

 function getBestFoodSaler(){
	$ci=& get_instance();
 	$f=$ci->session->userdata('from');
 	$t=$ci->session->userdata('to');
 $query="SELECT employees.firstname,employees.lastname,SUM(foodSales.price) AS food_sales FROM foodSales inner join employees on foodSales.saler_id=employees.employee_id WHERE foodSales.status=1 AND foodSales.sale_time BETWEEN '$f' AND '$t'";
	$result=$ci->db->query($query);
	$res=$result->result();
	return $res;
}
