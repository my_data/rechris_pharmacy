<?php
 defined('BASEPATH') or exit('No direct script access allowed');

 /**
 * 
 */
 class Accomodation_model extends CI_Model{
 	
 	public function __construct()
 	{
 		
 	}

 	function getRooms(){
 		$this->db->trans_begin();
 		$query="SELECT * FROM rooms ORDER BY name ASC";
 		if($results=$this->db->query($query)){
 			$this->db->trans_commit();
 			return $results->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}

 	}
 	function roomStatus($qry){
 		$this->db->trans_begin();
 		$result=$this->db->query($qry);
 		if($result->num_rows()==1){
 			$this->db->trans_commit();
 			return true;
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}

 	}
 	function filter($no){
 		$no=(int)$no;
 		$this->db->trans_begin();
 		$query="SELECT * FROM rooms ORDER BY name ASC LIMIT $no";
 		if($results=$this->db->query($query)){
 			$this->db->trans_commit();
 			return $results->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}

 	}

 	function saveRoom($query){
 		$this->db->trans_begin();
 		$this->db->query($query);
 		if($this->db->affected_rows()>0){
 			$this->db->trans_commit();
 			return 1;
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}

 	function saleroom($query){
 		$this->db->trans_begin();
 		$this->db->query($query);
 		if($this->db->affected_rows()>0){
 			$this->db->trans_commit();
 			return 1;
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}

 	function search($query){
 		$this->db->trans_begin();
 		if($result=$this->db->query($query)){
 			$this->db->trans_commit();
 			return $result->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}

 	function get_item_particular($query)
	{
		$this->db->trans_begin();
		if($success = $this->db->query($query)){
			$this->db->trans_commit();
			return $success->result();
		}
		else{
			$this->db->trans_rollback();
		}	
	}

	function update($query)
	{
		$this->db->trans_begin();
		if($this->db->query($query)){
			$this->db->trans_commit();
			return 1;
		}
		else{
			$this->db->trans_rollback();
			return false;
		}	
	}

	function delete_room($id=0)
	{
		$query="DELETE FROM rooms WHERE room_id='$id'";
		$this->db->trans_begin();
 		if($this->db->query($query)){ 
 			$this->db->trans_commit();
 			return 1;		
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
	}
	function delete_book($query){
		$this->db->trans_begin();
		$this->db->query($query);
 		if($this->db->affected_rows()>0){ 
 			$this->session->set_userdata('success','Booking Removed Successful');
 			$this->db->trans_commit();
 			return 1;		
 		}
 		else{
 			$this->session->set_userdata('fail','Sorry Fail');
 			$this->db->trans_rollback();
 			return FALSE;
 		}
	}

	function soldRooms(){
		$q="SELECT rooms.name,soldroom.checkout_time,rooms.fee,rooms.category,soldroom.status,soldroom.roomcustomer,soldroom.price,soldroom.day,soldroom.roomsale_id,soldroom.room_ID,employees.firstname,employees.lastname FROM soldroom  inner join rooms on rooms.room_id=soldroom.room_ID inner join employees on employees.employee_id=soldroom.roomsaler_id WHERE rooms.status='FULL' AND soldroom.status=1 OR soldroom.status=2";
		$result=$this->db->query($q);
		if ($result->num_rows()>0) {
			return $result->result();
		}
		else{
			return false;
		}
	}
	function bookedRooms(){
		$q="SELECT room_booking.tras_id,rooms.name,rooms.fee,rooms.category,room_booking.customer,room_booking.day,room_booking.saler_id,room_booking.room_id,employees.firstname,employees.lastname,room_booking.booked_day FROM room_booking  inner join rooms on rooms.room_id=room_booking.room_id inner join employees on employees.employee_id=room_booking.saler_id";
		$result=$this->db->query($q);
		if ($result->num_rows()>0) {
			return $result->result();
		}
		else{
			return false;
		}
	}
	function get_roomsales(){
		$from=date("Y-m-d",time());
		$to=date("Y-m-d",time());
		if (isset($_SESSION['from'])) {
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		}
		$q="SELECT rooms.name,rooms.fee,rooms.category,soldroom.status,soldroom.roomcustomer,soldroom.price,soldroom.day,soldroom.roomsale_id,soldroom.room_ID,employees.firstname,employees.lastname FROM soldroom  inner join rooms on rooms.room_id=soldroom.room_ID inner join employees on employees.employee_id=soldroom.roomsaler_id WHERE soldroom.day BETWEEN '$from' AND '$to'";
		$result=$this->db->query($q);
		if ($result->num_rows()>0) {
			return $result->result();
		}
		else{
			return false;
		}
	}

	function all_rooms(){
		$query=$this->db->query("SELECT * FROM rooms");
		if ($query->num_rows()>0){

            return $query->result();
		}
		else{
			return false;
		}
	}

	function getRoomInfo($roomid){
		$qr="SELECT fee,status FROM rooms WHERE room_id=$roomid";
		$qr=$this->db->query($qr);
		if ($qr->num_rows()>0) {

            return $qr->result();
		}
		else{
			redirect('accomodation');
		}
	}
	function getRoomInfo_from_soldRooms($roomid,$sale_id){
		$qr="SELECT price FROM soldroom WHERE room_ID=$roomid AND roomsale_id=$sale_id";
		$qr=$this->db->query($qr);
		if ($qr->num_rows()>0) {

            return $qr->result();
		}
		else{
			redirect('accomodation');
		}
	}
	function roomGraph(){
		$saler_id=$this->session->userdata('person_id');
		$today=date("Y-m-d",time());
		$query=$this->db->query("SELECT  rooms.name as room,soldroom.price as roomprice FROM soldroom inner join rooms on rooms.room_id=soldroom.room_ID WHERE soldroom.roomsaler_id=$saler_id AND soldroom.day='$today'");
		if ($query->num_rows()>0) {
			foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
		}
		else{
			return false;
		}
	}
}