<?php
 defined('BASEPATH') or exit('No direct script access allowed');

 /**
 * 
 */
 class Drink extends CI_Model{
 	
 	public function __construct()
 	{
 		
 	}

 	function getall(){
 		$this->db->trans_begin();
 		$query="SELECT * FROM drinks ORDER BY name ASC";
 		if($results=$this->db->query($query)){
 			$this->db->trans_commit();
 			return $results->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}

 	}
 	function filter($no){
 		$no=(int)$no;
 		$this->db->trans_begin();
 		$query="SELECT * FROM drinks ORDER BY name ASC LIMIT $no";
 		if($results=$this->db->query($query)){
 			$this->db->trans_commit();
 			return $results->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}

 	}

 	function saveitem($query){
 		$this->db->trans_begin();
 		if($this->db->query($query)){
 			$this->db->trans_commit();
 			return 1;
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}

 	function saleitem($query){
 		$this->db->trans_begin();
 		if($this->db->query($query)){
 			$this->db->trans_commit();
 			return 1;
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}

 	function search($query){
 		$this->db->trans_begin();
 		$result=$this->db->query($query);
 		if($result->num_rows()>0){
 			$this->db->trans_commit();
 			return $result->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}
 	function search_sale($query){
 		$this->db->trans_begin();
 		$result=$this->db->query($query);
 		if($result->num_rows()>0){
 			$this->db->trans_commit();
 			return $result;
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}

 	function get_item_particular($query)
	{
		$this->db->trans_begin();
		if($success = $this->db->query($query)){
			$this->db->trans_commit();
			return $success->result();
		}
		else{
			$this->db->trans_rollback();
			return false;
		}	
	}
	function suspenditem_particular($query)
	{
		$this->db->trans_begin();
		if($success = $this->db->query($query)){
			$this->db->trans_commit();
			return $success;
		}
		else{
			$this->db->trans_rollback();
			return false;
		}	
	}
	
	function update_original_item_size($query)
	{
		$this->db->trans_begin();
		if($r=$this->db->query($query)){
			if ($this->db->affected_rows()>0) {
				$this->db->trans_commit();
				return $r;
			}
			else{
			$this->db->trans_rollback();
			return false;
		}	
		}
		else{
			$this->db->trans_rollback();
			return false;
		}	
	}

 	function update_item($query)
	{
		$this->db->trans_begin();
		$this->db->query($query);
		if($this->db->affected_rows()>0){
			$this->db->trans_commit();
			return 1;
		}
		else{
			$this->db->trans_rollback();
			return false;
		}	
	}

	function remove_item($id=0)
	{
		$query="DELETE FROM drinks WHERE item_id='$id'";
		$this->db->trans_begin();
 		$this->db->query($query);
		if($this->db->affected_rows()>0){
			$this->db->trans_commit();
			return 1;	
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
	}
	function delete_sale($sale_id=0,$item_id)
	{
		$query="DELETE FROM drinkSales WHERE sale_id=$sale_id AND item_id=$item_id";
		$this->db->trans_begin();
 		$this->db->query($query);
		if($this->db->affected_rows()>0){
			$this->db->trans_commit();
			return 1;	
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
	}

	function get_suspended_sales_drink($saler_id){
		$query="SELECT * FROM drinkSales INNER JOIN employees ON drinkSales.saler_id=employees.employee_id WHERE payment_type!='Cash'";
		$result=$this->db->query($query);
		if($result->num_rows()>0){
			return $result;
		}
		else{
			return false;
		}
	}
	
	function remove_suspended($query){
		$this->db->trans_begin();
		$this->db->query($query);
		if($this->db->affected_rows()>0){
			$this->db->trans_commit();
			return 1;
		}
		else{
			$this->db->trans_rollback();
			return false;
		}
	}

	function suspended_particular($query){
		if($result=$this->db->query($query)){
			return $result;
		}
		else{
			return false;
		}
	}


	function delete_suspended_drink($id){
		$this->db->trans_begin();
		$query="DELETE FROM drinkSales WHERE sale_id='$id' AND status=0";
		$this->db->query($query);
		if($this->db->affected_rows()==1){
			$this->db->trans_commit();
			return true;
		}
		else{
			$this->db->trans_rollback();
			return false;
		}
	}

	function update_sale($item_id,$new_sale_qty,$customer,$old_sale_qty,$sale_id){
		//Get item stock and then return the previous sale quantity before add new
		$this->db->trans_begin();
		$stock_obj=$this->db->query("SELECT * FROM drinks WHERE item_id=$item_id LIMIT 1");
		$stock_ob=$stock_obj->result();
		$stock=$stock_ob[0]->size;
		$retail_price=$stock_ob[0]->price;
		$new_price=(float)($retail_price*(int)$new_sale_qty);
		$stock=(int)$stock+$old_sale_qty;
		$new_stock=(int)$stock-$new_sale_qty;
		if ($new_stock<0) {
			redirect('counter');
		}
		$query_string1="UPDATE drinkSales SET amount=$new_sale_qty,customer='$customer',price=$new_price WHERE sale_id=$sale_id";
		$this->db->query($query_string1);
		if($this->db->affected_rows()>0){
			$this->db->trans_commit();
			$query_string="UPDATE drinks SET size=$new_stock WHERE item_id=$item_id";
			$this->db->query($query_string);
			if($this->db->affected_rows()>0){
					
				$this->db->trans_commit();
				return 1;
			}
			else{
				$this->db->trans_rollback();
				return false;
			}
		}
		else{
				$this->db->trans_rollback();
				return false;
			}
	}

	function received_drink($qry){
		$this->db->trans_begin();
		$this->db->query($qry);
		if ($this->db->affected_rows()>0) {
			$this->db->trans_commit();
			redirect(site_url('receiving'));
		}
		else{
			$this->db->trans_rollback();
			redirect(site_url('receiving'));
		}
	}

	function get_order_particular($qry){
		$this->db->trans_begin();
		$r=$this->db->query($qry);
		if ($r->num_rows()>0) {
			$this->db->trans_commit();
			return $r->result_array();
		}
		else{
			$this->db->trans_rollback();
			redirect(site_url('drinks_order'));
		}
	}

function get_nhif_price($item_id)
	{
		$query="SELECT NHIF FROM drinks WHERE item_id=$item_id";
		$result=$this->db->query($query);
		if ($result->num_rows()>0) {
			$this->db->trans_commit();
			$result= $result->result();
			return $result[0]->NHIF;
		}
		else{
			$this->db->trans_rollback();
			redirect('counter');
		}	
	}
function get_cash_price($item_id)
	{
		$query="SELECT price FROM drinks WHERE item_id=$item_id";
		$this->db->trans_begin();
		$result=$this->db->query($query);
		if ($result->num_rows()>0) {
			$this->db->trans_commit();
			$result= $result->result();
			return $result[0]->price;
		}
		else{
			$this->db->trans_rollback();
			$this->session->set_userdata('fail','There is the problem in payment type');
			redirect('counter');
		}
	}
function get_strategy_price($item_id)
	{
		$query="SELECT STRATEGY FROM drinks WHERE item_id=$item_id";
		$this->db->trans_begin();
		$result=$this->db->query($query);
		if ($result->num_rows()>0) {
			$this->db->trans_commit();
			$result= $result->result();
			return $result[0]->STRATEGY;
		}
		else{
			$this->db->trans_rollback();
			$this->session->set_userdata('fail','There is the problem in payment type');
			redirect('counter');
		}
	}
function get_tanesco_price($item_id)
	{
		$query="SELECT TANESCO FROM drinks WHERE item_id=$item_id";
		$this->db->trans_begin();
		$this->db->trans_begin();
		$result=$this->db->query($query);
		if ($result->num_rows()>0) {
			$this->db->trans_commit();
			$result= $result->result();
			return $result[0]->TANESCO;
		}
		else{
			$this->db->trans_rollback();
			$this->session->set_userdata('fail','There is the problem in payment type');
			redirect('counter');
		}
	}
function get_binge_price($item_id)
	{
		$query="SELECT BINGE FROM drinks WHERE item_id=$item_id";
		$this->db->trans_begin();
		$result=$this->db->query($query);
		if ($result->num_rows()>0) {
			$this->db->trans_commit();
			$result= $result->result();
			return $result[0]->BINGE;
		}
		else{
			$this->db->trans_rollback();
			$this->session->set_userdata('fail','There is the problem in payment type');
			redirect('counter');
		}
	}
	function drinksReport(){
		$saler_id=$this->session->userdata('person_id');
		$today=date("Y-m-d",time());
		$query=$this->db->query("SELECT  name,price FROM drinkSales WHERE payment_type='Cash'AND saler_id=$saler_id AND sale_time='$today'");
		if ($query->num_rows()>0) {
			foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
		}
		else{
			return false;
		}
	}


	function import_csv($data){
		$this->db->trans_begin();
		foreach ($data as $row) {
			$name=$row['drug_name'];
			$category=$row['formulation'];
			$amount=(int)$row['stock'];
			$price=(double)$row['cash_price'];
			$cost=(double)$row['cost_per_unit'];
			$package=$row['package'];
			$package_amount=(int)$row['package_amount'];
			$cost_per_unit_wholesale=(double)$row['cost_per_package'];
			$whole_sale_price=(double)$row['whole_sale_price'];
			$exp_date=date('Y-m-d',time());
			$query="INSERT INTO drinks VALUES('$name','$category','$amount','',$price,$cost,0,0,0,0,0,'$exp_date',0,'$package',$package_amount,$cost_per_unit_wholesale,$whole_sale_price)";
				if($this->db->query($query)){
		 			$this->db->trans_commit();
		 		}
		 		else{
		 			$this->db->trans_rollback();
		 		}
		}
		if ($this->db->affected_rows()>0) {
			$this->session->set_userdata('success','Operation Successful');
			redirect('drinks');
		}
		else{
			$this->session->set_userdata('fail','Sorry! Operation Failed contact Admin');
			redirect('drinks');
		}
				

	}

}