<?php
 defined('BASEPATH') or exit('No direct script access allowed');

 /**
 * 
 */
 class Income extends CI_Model{
 	
 	public function __construct()
 	{
 		
 	}

 	function getall(){
 		$this->db->trans_begin();
 		if($results=$this->db->get('income')){
 			$this->db->trans_commit();
 			return $results->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}

 	}
 	function filter_income($no){
 		$no=(int)$no;
 		$this->db->trans_begin();
 		$query="SELECT * FROM income ORDER BY day ASC LIMIT $no";
 		if($results=$this->db->query($query)){
 			$this->db->trans_commit();
 			return $results->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}

 	}

 	function saveData($query){
 		$this->db->trans_begin();
 		if($this->db->query($query)){
 			$this->db->trans_commit();
 			return 1;
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}

 	function search_income($query){
 		$this->db->trans_begin();
 		if($result=$this->db->query($query)){
 			$this->db->trans_commit();
 			return $result->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}


 	function get_income_particular($query)
	{
		$this->db->trans_begin();
		if($success = $this->db->query($query)){
			$this->db->trans_commit();
			return $success->result();
		}
		else{
			$this->db->trans_rollback();
		}	
	}
 	function update_income($query)
	{
		$this->db->trans_begin();
		if($this->db->query($query)){
			$this->db->trans_commit();
			return 1;
		}
		else{
			$this->db->trans_rollback();
			return false;
		}	
	}

	function remove_income($id=0)
	{
		$query="DELETE FROM income WHERE income_id='$id'";
		$this->db->trans_begin();
 		if($this->db->query($query)){
 			$this->db->trans_commit();
 			return 1;		
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
	}
 }