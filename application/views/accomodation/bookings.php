<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/datatables.min.css"/>
<div class="panel panel-primary">
	<div class="panel-heading"></div>
	<?php require_once(APPPATH.'/views/print.php');?>
	<div class="panel-body">
<div class="panel panel-default">
	<div class="panel-heading"><?php echo $this->lang->line('bookings',FALSE);?>
	</div>
	<div class="panel-body">
	<div class="col-md-12 col-lg-12 col-sm-12">
				<button type="button" onclick="printData('printArea')" class="btn btn-success pull-right"><i class="fa fa-print"></i></button>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 		<?php $this->load->view('breadcrumb');?>
			</div>
			<div id="printArea">
		<table class="table table-bordered table-hover" id="mytable2">
			<thead>
				<tr>
					<th><?php echo $this->lang->line('room_number',FALSE);?></th><th><?php echo $this->lang->line('category',FALSE);?></th><th><?php echo $this->lang->line('fee',FALSE);?></th><th><?php echo $this->lang->line('customer_name',FALSE);?></th><th><?php echo $this->lang->line('saler',FALSE);?></th><th><?php echo $this->lang->line('booked_on',FALSE);?></th><th>Booked Day</th><th><?php echo $this->lang->line('action',FALSE);?></th>
				</tr>
			</thead>
			<tbody>
			<?php

	if($r=$this->accomodation_model->bookedRooms()){
		foreach ($r as $v) {
			
		?>
				<tr>
					<td><?php echo $v->name?></td><td><?php echo $v->category?></td><td><?php echo number_format($v->fee);?></td><td><?php echo $v->customer;?></td><td><?php echo $v->firstname.' '.$v->lastname?></td><td><?php echo $v->day?></td><td><?php echo $v->booked_day;?></td>
					<td>
					    <a href="<?php echo site_url('accomodation/deleteBook/'.$v->tras_id);?>" class="btn btn-danger btn-sm" onclick="return confirm('Delete this booking?');"><i class="fa fa-trash"></i></a>
					</td>
				</tr>
		<?php
	}}
	?>
		</tbody>
		</table>
		</div>
	</div>
			
</div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>datatable/datatables.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#mytable2").DataTable();
			});
		</script>
</div>