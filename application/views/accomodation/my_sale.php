<script type="text/javascript">
	$(document).ready(function(){
		$("#detailed-sale").slideUp();
		$("#summary-table").slideUp();
	});
</script>
<div class="panel panel-primary">
	<div class="panel-heading">
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
		
		 <h4 style="text-align: center;color: green;"><?php echo lang('my_today_sale',FALSE);?></h4>
		 <!-- toggle detailed sales  -->
				 <script type="text/javascript">
				 	$(document).ready(function(){
				 		$("#toggle-table-summary").click(function(){
				 			$("#summary-table").slideToggle();
				 			$("#detailed-sale").slideUp();
				 		});
				 	});
				 </script>
		<div class="panel panel-default">
		<div class="panel-heading">
		<?php echo lang('summary',FALSE);?>
		</div>
			<div class="panel-body">
			<button type="button" id="toggle-table-summary" class="btn btn-success"><?php echo lang('view_button',FALSE);?></button>
			<div class="table-responsive" id="summary-table">
				 	<table class="table table-hover table-bordered table-striped">
				 		<thead>
				 			<tr>
				 				<th><?php echo lang('room',FALSE);?></th><th><?php echo lang('category',FALSE);?></th><th><?php echo lang('customer_name',FALSE);?></th><th><?php echo lang('cash',FALSE);?></th>
				 			</tr>
				 		</thead>
				 		<tbody>
			 		<?php 
			 		$my_cash=0;
				 $saler_id=(int)$this->session->userdata('person_id');
				 	$today=date("Y-m-d",time());
				 $query_sales=$this->db->query("SELECT * FROM soldroom inner join rooms on rooms.room_id=soldroom.room_ID WHERE soldroom.roomsaler_id=$saler_id AND soldroom.day='$today' AND soldroom.status=0 OR soldroom.status=1");
				 if($query_sales->num_rows()>0){ 
				 	foreach ($query_sales->result() as $sale) {
				 		$my_cash=$my_cash+$sale->price;
				 		?>
				 		<tr><td><?php echo $sale->name;?></td><td><?php echo $sale->category;?></td><td><?php echo $sale->roomcustomer;?></td><td><?php echo $sale->price;?></td></tr>
						<?php
						}
						?>
				<tr style="color: green;">
				 	<th colspan=""><h3><?php echo lang('my_pocket_cash');?></h3></th><th colspan=""><h3><?php echo number_format($my_cash);?></h3>
				 	</th>
				 </tr>
				<?php 

				}
				else{
						?><tr><td><div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							 No Record Today!
						</div></td></tr><?php
					
					} ?>
				</tbody>
				</table>
				 </div>
				 </div>
				 </div>
				 <!-- toggle detailed sales  -->
				 <script type="text/javascript">
				 	$(document).ready(function(){
				 		$("#toggle-table-detail").click(function(){
				 			$("#detailed-sale").slideToggle();
				 			$("#summary-table").slideUp();
				 		});

				 		$("#toggle-table-summary").click(function(){
				 			$("#detailed-sale").slideUp();
				 		});
				 	});
				 </script>
				 <!-- table for display credit sales -->
		<div class="panel panel-default">
		<div class="panel-heading">
		</div>
			<div class="panel-body">
				  <button type="button" id="toggle-table-detail" class="btn btn-warning"><?php echo lang('view_credit_sales');?></button>
				 <div class="table-responsive" id="detailed-sale"> 
				 	<table class="table table-hover table-bordered table-striped">
				 	<thead>
				 			<tr>
				 				<th><?php echo lang('room',FALSE);?></th><th><?php echo lang('category',FALSE);?></th><th><?php echo lang('customer_name',FALSE);?></th><th><?php echo lang('cash',FALSE);?></th>
				 			</tr>
				 		</thead>
				 		<tbody>
			 		<?php 
			 		$my_credit=0;
				 $saler_id=(int)$this->session->userdata('person_id');
				 $day=date("Y-m-d",time());
				 $query_sales=$this->db->query("SELECT * FROM soldroom inner join rooms on rooms.room_id=soldroom.room_ID WHERE soldroom.roomsaler_id=$saler_id AND soldroom.status=2 AND soldroom.day='$day'");
				 if($query_sales->num_rows()>0){ 
				 	foreach ($query_sales->result() as $sale) {
				 		$my_credit=$my_credit+$sale->price;
				 		?>
				 		<tr><td><?php echo $sale->name;?></td><td><?php echo $sale->category;?></td><td><?php echo $sale->roomcustomer;?></td><td><?php echo $sale->price;?></td></tr>
						<?php
						}
						?>
				<tr style="color: green;">
				 	<th colspan=""><h3><?php echo lang('my_pocket_cash');?></h3></th><th colspan=""><h3><?php echo number_format($my_credit);?></h3>
				 	</th>
				 </tr>
				<?php 

				}
				else{
						?><tr><td><div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							 No Record Today!
						</div></td></tr><?php
					
					} ?>
				</tbody>
				 	</table>	
				 </div>
				 </div>
		</div>
			</div>	
		</div>
	</div>
</div>