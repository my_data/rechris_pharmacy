<div class="panel panel-primary">
	<div class="panel-heading">
	<?php echo lang('sold_rooms_report');
	 $from=strtotime($this->session->userdata('from'));
				$to=strtotime($this->session->userdata('to'));
				$from=date('d/m/Y',$from);
				$to=date('d/m/Y',$to);
		?>
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
		<?php 
		$attr=array('class'=>'form-inline','role'=>'form');
		echo form_open('reports/soldrooms_summary',$attr);?>
		<div class="table">
			<table class="table table-sm">
				<thead>
					<tr>
						<th><div class="form-group">
						<?php echo $this->lang->line('today',FALSE);?>:<input type="checkbox" name="today" id="today" class="form-control input-sm">
						</div>
						</th>
						<th>
							<?php echo $this->lang->line('from',FALSE);?>
							<div class="input-group">
							<span class="input-group-addon input-sm"><i class="fa fa-calendar"></i></span>
							<input type="date" name="from" id="from" class="form-control">
							</div>
						</th>
						<th>
							<?php echo $this->lang->line('to',FALSE);?>
							<div class="input-group">
							<span class="input-group-addon input-sm"><i class="fa fa-calendar"></i></span>
							<input type="date" name="to" id="to" class="form-control">
							</div>
						</th>
						<th>
					<button type="submit" class="btn btn-warning btn-block btn-sm" name="show_rooms"><?php echo lang('generate_button');?>
						</button>
						</th>
					</tr>
				</thead>	
			</table>
		</div>
		</form>	
		<div class="panel panel-default">
			<?php require_once(APPPATH.'/views/print.php');?>
				<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
			<button type="button" onclick="printData('printArea')" class="btn btn-success pull-right"><i class="fa fa-print"></i></button>
			</div>
			</div>
			<div class="panel-body" id="printArea">
			<div class="table-responsive">
				 	<table border="1" class="table table-hover table-striped table-bordered table-sm">
				 		<thead>
				 		<caption style="color:black; padding-bottom:2%;"><center><h4><?php echo $this->lang->line('summary_rooms_sales').' '.'FROM'.' ('.$from.') '.'TO'.' ('.$to.')';?></h4></center></caption>
				 			<tr>
				 			<th>S/N:</th>
				 			<th><?php echo $this->lang->line('room_number',FALSE);?></th>
				 			<th><?php echo $this->lang->line('category',FALSE);?></th>
				 			<th><?php echo $this->lang->line('current_status',FALSE);?></th>
				 			<th><?php echo $this->lang->line('cash',FALSE);?></th>
				 			<th><?php echo $this->lang->line('credit',FALSE);?></th>
				 			<th><?php echo $this->lang->line('gross_profit',FALSE);?></th>
				 			</tr>
				 		</thead>
				 		<tbody>
				<?php
				$counter=1;
				$room_status='';
				$gross_profit_per_room=0;
				$totalCredit_per_room=0;
				$totalCash_per_room=0;
				$sum_credit_sales=0;
				$sum_cash_sales=0;
				$total_credit_sales=0;
				$total_cash_sales=0;
				$sum_gross_profit=0;
				$total_empty_rooms=0;
				$total_full_rooms=0;
				if (isset($all_rooms)){
					if ($all_rooms) {
					foreach ($all_rooms as $room) {
					$res=get_sold_room_particular($room->room_id,$this->session->userdata('from'),$this->session->userdata('to'));
					 foreach ($res as $item) {
					 	$totalCredit_per_room=get_room_totalCredit($room->room_id,$this->session->userdata('from'),$this->session->userdata('to'));
					 	//Summation of cash sales
					 	$totalCash_per_room=get_room_totalCash($room->room_id,$this->session->userdata('from'),$this->session->userdata('to'));
					 	//summation of credit sales
					 	$sum_credit_sales=$sum_credit_sales+$totalCredit_per_room;
					 	//summation of cash sales
					 	$sum_cash_sales=$sum_cash_sales+$totalCash_per_room;
					 	//summation of full rooms
					 	if ($room->status=="FULL") {
					 		$room_status="Checked-In";
					 		$total_full_rooms++;
					 		# code...
					 	}
					 	//summation of empty rooms
					 	if ($room->status=="EMPTY") {
					 		$room_status="Checked-Out";
					 		$total_empty_rooms++;
					 		# code...
					 	}
					 	//Finding of gross profit per room	
					 		$gross_profit_per_room=$totalCredit_per_room+$totalCash_per_room;
					 	//summation of gross profit
					 	$sum_gross_profit=$sum_gross_profit+$gross_profit_per_room;
					?>	 			
				 			<tr>
				 			<td><?php echo $counter;?></td>
				 			<td><?php echo $room->name;?></td><td><?php echo $room->category;?></td>
				 			<td><?php echo $room_status;?></td>
				 			<td><?php echo number_format($totalCash_per_room);?>/=</td>
				 			<td><?php echo number_format($totalCredit_per_room);?>/=</td>
				 			<td><?php echo number_format($gross_profit_per_room);?>/=</td>
				 			</tr>
				 		
					<?php
				}
				$counter++;
				}
				?>
				<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
				<tr>
				<td colspan="2">Total</td><td></td><td></td>
				<td><?php echo number_format($sum_cash_sales);?>/=</td>
				<td><?php echo number_format($sum_credit_sales);?>/=</td>
				<td><?php echo number_format($sum_gross_profit);?>/=</td>
				</tr>
				<tr>
				<td colspan="2">Total Empty Rooms</td><td></td><td></td>
				<td></td>
				<td></td>
				<td><?php echo $total_empty_rooms;?></td>
				</tr>
				<tr>
				<td colspan="2">Total Occupied Rooms</td><td></td><td></td>
				<td></td>
				<td></td>
				<td><?php echo $total_full_rooms;?></td>
				</tr>	
				<?php
				}
				else{
						?><tr><td><div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							 Select Record!
						</div></td></tr><?php
					
					}
				}
				else{
						?><tr><td><div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							 Select Record!
						</div></td></tr><?php
					
					} 
					?>
				</tbody>
				</table>
				 </div>
			</div>	
		</div>
	</div>
</div>