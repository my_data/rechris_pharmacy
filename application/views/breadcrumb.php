<style type="text/css">
    .stepwizard-step p {
    margin-top: 10px;    
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;     
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
    
}

.stepwizard-step {    
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}
</style>
<script>
function goBack() {
    window.history.back();
}
</script>
<?php
   // $url=previous_url();
?>
<div class="stepwizard">
    <div class="stepwizard-row">
        <div class="stepwizard-step">
            <a href="<?php echo site_url('home');?>"><button type="button" class="btn btn-default btn-circle"><i class="fa fa-home"></i></button></a>
        </div>
        <div class="stepwizard-step">
            <button type="button" class="btn btn-default btn-circle" onclick="goBack();"><i class="fa fa-angle-double-left"></i></button>
        </div>
    </div>
</div>