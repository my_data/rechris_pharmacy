<script type="text/javascript" src="<?php echo base_url(); ?>print_plugin/jQuery.print.js"></script>
<?php
$attrib=array('role'=>'form','class'=>'form-inline','id'=>'sale-form');
 echo form_open('counter/sale',$attrib);
 ?>
        <h4 style="text-align: center;color:blue;">
           <!--  <i class="fa fa-cart-plus fa-15x" style="font-size:60px"></i> -->
           INVOICE
        </h4>
        <?php require_once(APPPATH.'/views/print.php');?>
        <div class="table-responsive" id="printArea">
                <table class="table table-bordered" >
                        <thead>
                                <?php
                                    $receipt_no='';
                                ?>
                                <tr>
                                    <th>Drug Name</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th class="no-print">...</th>
                                </tr>
                        </thead>
                        <tbody>
           
                        <?php 
                        if(!$this->cart->contents()){
                                ?>
                                 <tr>
                                    <td colspan="4">
                                        <div class="alert alert-info">
                                                <button type="button" class="close no-print" data-dismiss="alert" aria-hidden="true">
                                                &times;</button>
                                               <strong>No content</strong>
                                        </div>
                                 </td>
                                </tr>
                                <?php
                        }
                        foreach ($this->cart->contents() as $item) {
                               if ($this->cart->total()!='') {
                                   $receipt_no=time();
                                }
                          ?>
                        <tr>
                             <td><?php echo $item['name'];?></td>
                            <td><?php  echo $item['qty'];?></td>
                            <td><?php echo $this->cart->format_number($item['price']); ?></td>
                             <td class="no-print">
                                <a href="<?php echo site_url('counter/remove_cart_item/'.$item['rowid'])?>">
                                <i class=" fa fa-remove" style="color: red;"></i></a>
                            </td>

                        </tr>
                          <?php     
                        }
                        ?>
        <tr style="color: green">
            <td >Total Price</td>
            <td></td><td><?php echo $this->cart->format_number($this->cart->total());?>/=</td><td class="no-print"></td>
        </tr>
        <!-- <tr>
            <td>Customer/Patient
            <input type="text" name="customer">
            </td><td></td><td></td><td class="no-print"></td>
        </tr> -->
        </tbody> 
        <tfoot class="footer">
            <tr>
                <td>Receipt No: <?php echo $receipt_no;?></td><td></td><td></td><td class="no-print"></td>
            </tr>
     </tfoot>                   
     </table>
    </div>
            
        <input type="hidden" name="payment_type" class="form-control"  
      value="<?php if(isset($_SESSION['payment_type'])){ echo $_SESSION['payment_type']; }?>" required>
    <div class="btn-group-sm">  
        <button type="button" class="btn btn-default  btn-sm" id="printProformer">
        Proformer Invoice
        </button>
        <button type="submit" class="btn btn-success  btn-sm" name="sale" id="printReceipt">
        Complete Sale
        </button>
    </div>
</form>

