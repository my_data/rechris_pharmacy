<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/datatables.min.css"/>
<div class="panel panel-default">
			<div class="panel-body">		
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Insurances Sales</h3>
				</div>
				<?php require_once(APPPATH.'/views/print.php');?>
				<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<button type="button" onclick="printData('printArea')" id="printView" class="btn btn-success btn-sm pull-right"><i class="fa fa-print"></i></button>
			</div>
			</div>
				<div class="panel-body">

			<div class="table-responsive" id="printArea">
				 	<table class="table table-hover table-striped table-bordered" id="mytable2">
				 		<thead>
				 			<tr>
				 				<th><?php echo $this->lang->line('name',FALSE);?></th>
				 				<th><?php echo $this->lang->line('category',FALSE);?></th>
				 				<th>Qty Sold</th>
				 				<th><?php echo $this->lang->line('sold_date',FALSE);?></th>
				 				<th>Patient</th>
				 				<th>Sale Price</th>
				 				<th>Payment type</th>
				 				<th>Saler</th>
				 				<th>...</th>
				 			</tr>
				 		</thead>
				 		<tbody>
				<?php
				if (isset($suspended)) {
					$total_sale=0;
					if (!$suspended){
							?>
				 		<tr><td colspan=""><div class="alert alert-danger" style="text-align: center;">
				 			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				 			No Records Found!
				 		</div></td></tr>
				
				 <?php
						}else{
					foreach ($suspended->result() as $item) {
	
					?>
				 		<tr>
				 		<td style="color: green"><?php echo $item->name;?></td>
				 		<td style="color: orange"><?php echo $item->category;?></td>
				 		<td><?php echo $item->amount;?></td>
				 		<td><?php echo $item->sale_time;?></td>
				 		<td><?php echo $item->customer;?></td>
				 		<td style="color:blue"><?php echo $item->price;?></td>
				 		<td style="color: green"><?php echo $item->payment_type;?></td>
				 		<td><?php echo $item->firstname.' '.$item->lastname;?></td>
				 		<td><a href="<?php echo site_url('counter/unsuspend/'.$item->sale_id);?>" class="btn btn-success btn-sm">Clear</a></td>
				 		</tr>
				 		
					<?php
					$total_sale=$total_sale+($item->amount*$item->price);
				}}
				?>
				<?php }
				?>
				<tr style="color: red">
				<td>Total Sales (Tshs)</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				<td><?php echo number_format($total_sale);?>/=</td>
				</tr>
				</tbody>
				</table>
		</div>
				</div>
			</div>
			</div>
</div>

	