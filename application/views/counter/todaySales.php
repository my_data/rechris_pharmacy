<div class="panel panel-primary">
	<div class="panel-body">
		 <h4 style="text-align:center;"><?php echo $this->lang->line('today_sale',FALSE);?></h4>
				
		<div class="panel panel-default">
		<div class="panel-heading">
		</div>
			<div class="panel-body">
			<button type="button" id="toggle-table-summary" class="btn btn-default">
			<?php echo $this->lang->line('summary',FALSE);?>	
			</button>
			<div class="table-responsive" id="summary-table">
				 	<table class="table table-bordered">
				 		<tbody>
				<?php
				if (isset($cash) && isset($sold_item_name)){
					if ($sold_item_name) {
					$total_sale=$cash->row();
				
				?>
				<tr style="color: green;">
				 	<th colspan=""><h3>My pockect Cash(Tshs):</h3></th>
				 	<th colspan=""><h3><?php echo number_format($total_sale->value_sum);?>/=</h3>
				 	</th>
				 </tr>
				<?php 

				}}
				else{
						?><tr><td><div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							 Select Record!
						</div></td></tr><?php
					
					} ?>
				</tbody>
				</table>
				 </div>
				 </div>
				 </div>
				 <!-- toggle detailed sales  -->
				
				 <!-- table for display drink sales -->
				 <div class="panel panel-default">
		<div class="panel-heading">
		</div>
			<div class="panel-body">
				  <button type="button" id="toggle-table-detail" class="btn btn-default">Detailed</button>
				 <div class="table-responsive" id="detailed-sale"> 
				 	<table class="table table-hover">
				 		<thead>
				 			<tr>
				 				<th><?php echo $this->lang->line('name',FALSE);?></th>
				 				<th><?php echo $this->lang->line('category',FALSE);?></th>
				 				<th><?php echo $this->lang->line('customer_name',FALSE);?></th>
				 				<th><?php echo $this->lang->line('quantity_sold',FALSE);?></th>
				 				<th><?php echo $this->lang->line('price',FALSE);?></th>
				 				<th>Payment Type</th><th>...</th>
				 			</tr>
				 		</thead>
				 		<tbody>
				 		<?php 
				 			$saler_id=(int)$this->session->userdata('person_id');
				 			$day=date("Y-m-d",time());
				 $query_sales=$this->db->query("SELECT * FROM drinkSales WHERE saler_id=$saler_id AND payment_type='Cash' AND sale_time BETWEEN '$day' AND '$day'");
				 if($query_sales->num_rows()>0){ 
				 	foreach ($query_sales->result() as $sale) {
				 		?>
				 			<tr>
				 			<?php
				 			$attrib=array('class'=>'form-inline','role'=>'form');
				 			echo form_open('',$attrib);
				 			?>
				 			<td><input type="text" class="form-control" 
				 			value="<?php echo $sale->name; ?>" readonly >
				 			</td>
				 			<td><input type="text" name="category" class="form-control" 
				 			value="<?php echo $sale->category; ?>" readonly >
				 			</td>
				 			<td><input type="text" name="customer" class="form-control" 
				 			value="<?php echo $sale->customer; ?>" required="required" readonly></td>
				 			<input type="hidden" name="old_sale_qty" value="<?php echo $sale->amount; ?>" required="required">
				 			<td><input type="number" min="1" name="new_sale_qty" class="form-control"
				 			value="<?php echo $sale->amount; ?>" required="required" readonly></td>
				 			<td><input type="text" class="form-control" 
				 			value="<?php echo $sale->price; ?>" readonly>
				 			</td>
				 			<td><input type="text" class="form-control" 
				 			value="<?php echo $sale->payment_type; ?>" readonly>
				 			</td>
				 			<input type="hidden" name="drink_id" value="<?php echo $sale->item_id; ?>" required="required">
				 			<input type="hidden" name="sale_id" value="<?php echo $sale->sale_id; ?>" required="required">
				 			</form>
				 			</tr>
					 		<?php
					 			}} 
					 		?>
				 			<tr><td><a href="<?php echo base_url('home');?>" class="btn btn-danger"><?php echo $this->lang->line('close',FALSE);?></a></td></tr>
				 		</tbody>
				 	</table>
				 </div>
				 </div>
				 </div>
				 </div>
			</div>	
		</div>
	</div>
</div>