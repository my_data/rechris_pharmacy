<div class="row">
		<div class="col-lg-7 col-sm-7 col-md-7 col-xs-12">
			<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><strong>Counter-Whole Sale</strong></h3>
		</div>
		<div class="panel-body">
		<script type="text/javascript">
				$(document).ready(function(){
					$("#search").keyup(function(){
					var value=this.value;
					if (value.length>3 && $.isNumeric(value)){
						window.location.href="<?php echo site_url('counter/search/')?>"+value;
						}	
					});	
				});
				$(document).ready(function(){
					$("#search").focus(function(){
					var value=this.value;
					if (value.length>3){
						window.location.href="<?php echo site_url('counter/search/')?>"+this.value;
						}	
					});	
				});
		</script>
		<div class="table-responsive">
			<table class="table table-bordered">
				<tbody>
					<tr>
						<td>
							<select name="Sale Mode" id="sale_mode" class="form-control input-sm">
								<option value="">---Sale Mode---</option>
								<option value="retail_sale">Retail Sale</option>
								<option value="whole_sale">Whole Sale</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="table-responsive">
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>
						<?php
					$attrib=array('class'=>'form-inline', 'autocomplete'=>"off");
					 echo form_open('counter/search',$attrib);
					 ?>	
			    	<div class="input-group col-lg-12 col-sm-12 col-md-12 col-xs-12">
				    		<input list="drugs" class="form-control mr-sm-2" type="search" id="search" placeholder="Scan Item" aria-label="Search" name="keyword" autofocus="autofocus"/>
					      <span class="input-group-btn">
					        <button class="btn btn-secondary" name="search" type="submit"><i class=" fa fa-search"></i></button>
					      </span>
 					</div>
 					<datalist id="drugs">
			    	<?php 
			    		$res=$this->db->query("SELECT name FROM drinks ORDER BY name ASC");
			    		if ($res->num_rows()>0) {
			    				foreach($res->result() as $res){ ?>
			    					<option value="<?php echo $res->name;?>">
			    			<?php }
			    			}
			    			?>		 
					</datalist>
 					</form>
 					</th>
					<th>
						<form class="form-inline" method="POST" action="<?php echo site_url('counter/todaySales');?>">
			    		<button type="submit" class="btn btn-success btn-sm col-lg-12 col-sm-12 col-md-12 col-xs-12" name="todaySales">My Wallet</button>
			  			</form>
			  		</th>
					<th>
					<form class="form-inline" method="POST" action="<?php echo site_url('counter/suspended');?>">
			    		<button type="submit" class="btn btn-primary btn-sm btn-sm col-lg-12 col-sm-12 col-md-12 col-xs-12" name="suspended">Ensurance Sales</button>
			  		</form>
					</th>
				</tr>
			</thead>
		</table>
	</div>
		<div class="table-responsive">
			<table class="table table-striped table-hover table-bordered table-responsive">
				<thead>
				<tr>
					<th>Drug Name</th>
					<th>Package</th>
					<th><?php echo $this->lang->line('stock',FALSE);?></th>
					<th>Price</th>
					<th>Sale QTY</th>
				</tr>
				</thead>
				<tbody>
					<?php
					$attrib=array('class'=>'form-inline','role'=>'form');
					if(isset($result)){ 
					foreach ($result as $item){
					echo form_open('counter/add_to_cart',$attrib);
					?>
					<tr>
					<td colspan="">
					<div class="form-group">
					<input type="text" class="form-control col-lg-12 col-sm-12 col-md-12 col-xs-12" name="itemname" value="<?php echo $item->name;?>" readonly="readonly" size="60"/>
					</div>
					</td>
					<td>
					<div class="form-group">
					<input type="text" class="form-control col-lg-12 col-sm-12 col-md-12 col-xs-12" name="package" value="<?php echo $item->package;?>" readonly="readonly" />
					</div>
					</td>
					<td>
					<div class="form-group">
					<input type="text" class="form-control col-lg-12 col-sm-12 col-md-12 col-xs-12" name="package_amount" value="<?php echo $item->package_amount;?>" readonly="readonly" />
					</div>
					</td>
					<td>
					<div class="form-group">
					<input type="number" class="form-control col-lg-12 col-sm-12 col-md-12 col-xs-12" value="<?php echo $item->whole_sale_price;?>" id="price" name="wholesale_price" min="1"/>
					</div>
					</td>
					<td>
					<div class="form-group">
					<input type="number" class="form-control col-lg-12 col-sm-12 col-md-12 col-xs-12" name="qty" id="amount" required="required" min="1" />
					<input type="hidden" name="item_id" value="<?php echo $item->item_id;?>" required="required" />
					<input type="hidden" name="wholesale_price" value="<?php echo $item->whole_sale_price;?>" required="required" />
					</div>
					</td>
					</tr>
					<tr>
					<td colspan="3">
					<button type="submit" class="btn btn-success btn-block btn-sm col-lg-12 col-sm-12 col-md-12 col-xs-12" name="add"><i class="fa fa-plus"></i>&nbsp Add to Sale</button>
					</td>
					</tr>
					</form>
					<?php	
					}}
					else{
						?>
						<tr><td colspan="5"><div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<strong>No Recod Selected</strong>
						</div></td></tr>
						<?php
					}
					?>
				</tbody>	
				</table>	
			</div>	
		</div>
	</div>
		
		</div>
		<div class="col-lg-5 col-sm-5 col-md-5 col-xs-12 pull-right">
		<div class="panel panel-primary">
			<div class="panel-heading"></div>
			<div class="panel-body">
				<div class="well well-lg">
				<?php require_once('shopping_cart.php');?>
				</div>
			</div>
		</div>
			
		</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
			$("#sale_mode").change(function() {
				var sale_mode=this.value;
				if (sale_mode=='whole_sale') {
					window.location.href="<?php echo site_url('counter/whole_sale')?>";
				}
				else if(sale_mode=="retail_sale") {
					window.location.href="<?php echo site_url('counter')?>";
				}
				else{
					<?php $this->session->set_userdata('sale_mode','retail_sale');?>
					window.location.href="<?php echo site_url('counter')?>";
				}
			});
	});
</script>