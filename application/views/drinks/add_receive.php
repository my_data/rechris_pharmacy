<script type="text/javascript">
	$(document).ready(function(){
		$("#concel").click(function(){
			window.location.href="<?php echo site_url('receiving');?>";
		});

	});
</script>
<style type="text/css">
	input{
		text-align: center;
	}
</style>
<div class="panel panel-default">
<div class="panel-body">

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Receive Drug</h3>
	</div>
	<div class="panel-body">
		<?php

			$attrib=array('role'=>'form');
									echo form_open('receiving/add',$attrib);?>
									<table class="table table-bordered">
										<tbody>
											<tr>
										<td>
										<div class="form-group col-lg-5 col-md-5">
											<label for="">Name</label>
											<input type="text" class="form-control"  name="name" readonly="readonly" value="<?php echo $row[0]->name;?>">
										</div>	
										</td>
											<td>		
										<div class="form-group col-lg-5 col-md-5">
											<label for="">Formulation</label>
											<input type="text" class="form-control" value="<?php echo $row[0]->category;?>" readonly />
										</div>
										</td>
										</tr>
										<tr>
										<td>
											<div class="form-group col-lg-5 col-md-5">
											<label for="">Quantity Per Unit</label>
											<input type="number" class="form-control"  name="quantity" required="required" min="1">
										</div>
										</td>
										<td>
											
										<div class="form-group col-lg-5 col-md-5">
											<label for=""><?php echo lang('item_cost');?></label>
											<input type="number" class="form-control"   name="cost" required="required" value="<?php echo $row[0]->cost;?>">
										</div>
										</td>
											</tr>
											<tr>
												<td>
												<button type="button" class="btn btn-danger btn-block btn-sm" id="concel">Back</button>
												</td>
												<td>
													<button type="submit" class="btn btn-success btn-block btn-sm" name="receive_item">
							<i class="fa fa-plus"></i>
							</button>
												</td>
											</tr>
										</tbody>
									</table>
										
										
										<input type="hidden" name="item_id" required="required" value="<?php echo $row[0]->item_id;?>" />
										<input type="hidden" name="old_cost" required="required" value="<?php echo $row[0]->cost;?>" />
										<input type="hidden" name="old_stock" required="required" value="<?php echo $row[0]->size;?>"/>
	</div>
</div>
	</div>
</div>