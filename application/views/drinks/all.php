<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">
					<strong><?php echo $this->lang->line('drinks_inventory',FALSE);?></strong>
				</h3>
			</div>
			<div class="panel-body">
		<!-- 	first table -->
			<div class="table-responsive">
			<table class="table table-bordered">
				<tr>
			<!-- uploading excel file -->
					<td> 
							<form action="<?php echo site_url('drinks/import');?>" class="form-inline" method="post" enctype="multipart/form-data">
							<table>
								<tr>
									<td><label>Import(Excel/Csv)</label>
										<div class="input-group">
											
										   <input type="file" class="form-control input-sm" name="drugs" accept=".csv,.xls" required />
										   <span class="input-group-btn">
										        <button type="submit" name="upload" class="btn btn-sm btn-default">Upload</button>
										   </span>
										</div>
									</td>
									
								</tr>
							</table>
						</form>
					</td>
			<!-- 	end of aploading -->
					<td>
					<div>
					<a class="btn btn-success pull-right btn-sm" data-toggle="modal" href='#modal-id' style="border-radius:50%; ">
						<i class="fa fa-plus"></i>&nbsp Add Drug
					</a>
					<div class="modal fade" id="modal-id">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" style="text-align: center;">
										<span><img src="<?php echo base_url(); ?>images/7.png" class="img-responsive img-circle img-fluid" alt="Image" style="width:10%;"> Add New Drug</span>
									</h4>
								</div>
								<div class="modal-body">
									<?php 
									$attrib=array('role'=>'form');
									echo form_open('drinks/save',$attrib);?>
								<div class="table-responsive">
									<table class="table table-bordered">
										<caption>Drug Particular</caption>
										<tbody>
											<tr>
												<td>
												<div class="form-group">
											<label for=""><i style="color:red">*</i><?php echo $this->lang->line('drink',FALSE);?></label>
											<input type="text" class="form-control input-sm"  name="name" required="required">
										</div>
										</td>
										<td>
										<div class="form-group">
											<label for="category"><i style="color:red">*</i>
											<?php echo $this->lang->line('category',FALSE);?></label>
											<select name="category" id="input" class="form-control">
												<option value="">---Select---</option>
												<option value="Tablet">Tablet</option>
												<option value="Capsules">Capsules</option>
												<option value="Pessaries">Pessaries</option>
												<option value="Injection">Injection</option>
												<option value="Syrup">Syrup</option>
												<option value="Cream">Cream</option>
												<option value="Gel">Gel</option>
												<option value="Ointment">Ointment</option>
												<option value="Drops">Drops</option>
												<option value="Powder">Powder</option>
												<option value="Others">Others</option>
											</select>
										</div>
										</td>
										<td>
											<div class="form-group">
												<i style="color: red">*</i><label>Expire Date</label>
												<input type="date" name="expire_date" id="input" class="form-control" required>
											</div>
										</td>
										</tr>
										<tr>
										<td>
										<div class="form-group">
											<i style="color: red">*</i><label for="">Total Unit</label>
											<input type="number" class="form-control input-sm"   name="amount" required="required" step="0.01" min="0">
										</div>
										</td>
										<td>
										<div class="form-group">
											<i style="color: red">*</i><label for="">Re-order Level</label>
											<input type="number" class="form-control input-sm"   name="reorder_level" required="required" step="0.01" min="0">
										</div>
										</td>
										<td>
										<div class="form-group">
											<i style="color: red">*</i><label for=""><?php echo $this->lang->line('item_cost',FALSE);?></label>
											<input type="number" step="0.01" class="form-control input-sm"   name="cost" required="required" min="1">
										</div>
										</td>
										</tr>
								</table>
							</div>

						<!-- 	<div class="table-responsive">
							<table class="table table-bordered">
								<tbody>
										<caption><strong>For whole Sale</strong></caption>
										<tr>
											<td>
												<div class="form-group">
													<label for="Package">
													Package</label>
													<select name="package" id="input" class="form-control">
														<option value="">---Select---</option>
														<option value="Box">Box</option>
														<option value="Tin">Tin</option>
														<option value="Bottles">Bottles</option>
														<option value="Others">Others</option>
													</select>
												</div>
											</td>
									
									<td>
										<div class="form-group">
											<label for="package_amount">Amount
											</label>
											<input type="number" name="package_amount" class="form-control input-sm" min="1">
										</div>
									</td>
									<td>
										<div class="form-group">
											<label for="cost_per_unit_wholesale">Cost Per Unit
											</label>
											<input type="number" name="cost_per_unit_wholesale" class="form-control input-sm" min="1">
										</div>
									</td>
										<td>
										<div class="form-group">
											<label for="whole_sale_price">Whole sale Price
											</label>
											<input type="number" name="whole_sale_price" class="form-control input-sm" min="1">
										</div>
									</td>

										</tr>
								</tbody>
							</table>
						</div> -->
							<div class="table-responsive">
									<table class="table table-bordered">
										<caption><strong>Retail Prices</strong></caption>
										<tbody>
										<tr>
											<td>
											<div class="form-group">
											<i style="color:red">*</i><label for="">Cash Price</label>
											<input type="number" class="form-control input-sm" name="price" required="required" step="0.01" min="1">
										</div>
										</td>
										<!-- <td>
											<div class="form-group"><i style="color:red">*</i>
											<label for="">NHIF Price</label>
											<input type="number" class="form-control input-sm" name="NHIF" required="required" step="0.01" min="1">
										</div>
										</td>
										<td>
											<div class="form-group"><i style="color:red">*</i>
											<label for="">TANESCO Price</label>
											<input type="number" class="form-control input-sm" name="TANESCO" required="required" step="0.01" min="1">
										</div>
										</td>
										</tr>
										<tr>	
										<td>
											<div class="form-group"><i style="color:red">*</i>
											<label for="">BUNGE Price</label>
											<input type="number" class="form-control input-sm" name="BUNGE" required="required" step="0.01" min="1">
										</div>
										</td>
										<td>
											<div class="form-group"><i style="color:red">*</i>
											<label for="">STRATEGY Price</label>
											<input type="number" class="form-control input-sm" name="STRATEGY" required="required" step="0.01" min="1">
										</div>
										</td> -->
										<td>
											<div class="form-group">
											<button type="submit" name="save_item" class="btn btn-success btn-block"><i class="fa fa-plus"></i>Save</button>
											</div>
										</td>
										</tr>
									</tbody>
									</table>
							</div>
						</form>	
								</div>
								
							</div>
						</div>
					</div>
				</td>
				</tr>
		</table>
	</div>
<!-- end of first table -->

		<div class="table-responsive">
		<table class="table table-striped table-bordered w-auto" id="mytable1">
			<thead>
				<tr>
					<th>Drug Name:</th>
					<th><?php echo $this->lang->line('category',FALSE);?>:</th>
					<th><?php echo $this->lang->line('stock',FALSE);?>:</th>
					<th>Cash Price:</th>
					<!-- <th>NHIF:</th>
					<th>Strategy:</th>
					<th>Tanesco:</th>
					<th>Bunge:</th> -->
					<th>Expire:</th>
					<th>Cost/Unit:</th>	
					<th>Package:</th>
					<th>Package Amount:</th>
					<th>Cost/Package:</th>
					<th>Wholesale Price:</th>
					<th>Capital:</th>
					<th><?php echo $this->lang->line('action',FALSE);?>:</th>
				</tr>
			</thead>
			<tbody>
			<?php
					$reorder='';
					$expire='';
				foreach ($item_result as $row) {
					if($row->size<=$row->reorder_level){
					 	$reorder='<span class="label label-danger">Re-order!</span>';
					 }
					 if($row->size>$row->reorder_level){
					 	$reorder='';
					 }
					 //check expire date of an item
						$expire_date = new DateTime($row->expire_date);
						$today = new DateTime();
						$diff = $today->diff($expire_date)->format("%a");
						if($diff<=90){
							$expire='<i class="fa fa-spinner fa-spin" style="color:red;"></i>';
						}
						if($diff>90){
							$expire='';
						}
						
			?>

				<tr>
					<td style="color: green;">
						<?php echo $row->name.' '.$reorder;?>

					</td>
					<td style="color: blue;"><?php echo $row->category;?>
					</td>
					<td style="color: green;"><?php echo $row->size;?>
					</td>
					<td style="color: blue;"><?php echo number_format($row->price);?>/=
					</td>
					<!-- <td><?php //echo number_format($row->NHIF);?>/=</td>
					<td><?php //echo number_format($row->STRATEGY);?>/=</td>
					<td><?php //echo number_format($row->TANESCO);?>/=</td>
					<td><?php //echo number_format($row->BINGE);?>/=</td> -->
					<td><?php echo $row->expire_date.''.$expire;?></td>
					<td><?php echo number_format($row->cost);?>/=</td>
					<td><?php echo $row->package;?></td>
					<td><?php echo $row->package_amount;?></td>
					<td><?php echo $row->cost_per_package;?></td>
					<td><?php echo $row->whole_sale_price;?></td>
					<td>
						<?php echo number_format(($row->cost*$row->size)+($row->package_amount*$row->cost_per_package));?>/=
					</td>
					<td>
					<a href="<?php echo site_url('drinks/update/'.$row->item_id);?>">
						<i class="fa fa-edit" style="color:green"></i>
					</a>
					<a href="<?php echo site_url('drinks/delete/'.$row->item_id);?>" onclick="return confirm('Are you sure you want to delete this item?');">
						<i class="fa fa-trash-o" style="color:red"></i>
					</a>
					<a href="<?php echo site_url('drinks/generate_barcode/'.$row->item_id);?>" target="_blank">			Barcode
					</a>
					</td>
				</tr>
					<?php
				}
				?>
				
			</tbody>
		</table>
		</div>
		</div>
	</div>
		</div>
	</div>
</div>
</div>
	