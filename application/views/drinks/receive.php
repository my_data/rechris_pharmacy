<script type="text/javascript">
$(document).ready(function() {
//filtering of item
$("#sortby").change(function(){
	window.location.href="<?php echo site_url('drinks/filter/')?>"+this.value;
});
//searching of item
$("#search").keyup(function(){
	var value=this.value;
	if (value.length>4) {
		window.location.href="<?php echo site_url('receiving/search/')?>"+this.value;
	}	
});
});
</script>
<div class="panel panel-default">
	<div class="panel-body">
	<div class="panel panel-default">
	<div class="panel-heading">
			<h3 class="panel-title">Receivings</h3>
		</div>
	<div class="panel-body">
		<div class="table-responsive">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>
					<?php
					$attrib=array('class'=>'form-inline');
					 echo form_open('receiving/search',$attrib);
					 ?>	
			    		<div class="input-group">
			    		<input class="form-control mr-sm-2" type="search" id="search" placeholder="Search Sale" aria-label="Search" name="keyword" autofocus="autofocus" />
				      <span class="input-group-btn">
				        <button class="btn btn-secondary" name="search_drink" type="submit"><i class=" fa fa-search"></i></button>
				      </span>
 					</div>
 					</form>
					</th>
					<th><select name="sortby" id="sortby" class="form-control">
						<option value="">Filter By</option>
						<option value="5">5</option>
						<option value="10">10</option>
						<option value="50">50</option>
						<option value="100">100</option>
						</select>
					</th>
				</tr>
			</thead>
		</table>
	</div>
	<div class="table-responsive">
		<table class="table table-striped table-bordered">
			<tbody>
			<tr><td>SN:</td><td><?php echo $this->lang->line('drink',FALSE);?></td>
			<td><?php echo $this->lang->line('category',FALSE);?></td>
			<td><?php echo $this->lang->line('stock',FALSE);?></td>
			<td><?php echo $this->lang->line('price',FALSE);?></td>
			<td><?php echo $this->lang->line('action',FALSE);?></td></tr>
				<?php
				if ($item_result!=null) {
					$count=0;
				foreach ($item_result as $row) {
					$count++;
					?>

				<tr>
					<td><?php echo $count;?></td>
					<td><?php echo $row->name;?></td>
					<td><?php echo $row->category;?></td>
					<td><?php echo $row->size;?></td>
					<td><?php echo $row->price;?></td>
					<td>
					<a href="<?php echo site_url('receiving/add/'.$row->item_id);?>">
					<i class="fa fa-plus" style="color:green"></i></a>
					</td>
				</tr>
					<?php
				}
			}
				?>
				
			</tbody>
			<tfoot>
				
			</tfoot>
		</table>
		</div>
		</div>
	</div>
</div>