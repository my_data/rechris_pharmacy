<script type="text/javascript">
	$(document).ready(function(){
		$("#concel").click(function(){
			window.location.href="<?php echo site_url('drinks');?>";
		});

	});
</script>
<style type="text/css">
	h6{
		text-align:left;
	}
	input{
		text-align: center;
	}
</style>
<div class="panel panel-default">
	 	<div class="panel-body">
	 <div class="panel panel-default">
	 	<div class="panel-heading">
	 		<h3 align="center"> Edit Drug</h3>
	 	</div>
	 	<div class="panel-body">
	 		
		<?php 
					$attrib=array('role'=>'form','class'=>'form');
						echo form_open('drinks/update',$attrib);?>
									
										<div class="panel panel-success">
											<div class="panel-heading">
												
											</div>
											<div class="panel-body">
												<div class="form-group col-lg-5  col-md-5">
											<label for=""><?php echo $this->lang->line('drink',FALSE);?></label>
											<input type="text" class="form-control"  name="name" required="required" value="<?php echo $row[0]->name;?>">
										</div>
										<div class="form-group col-lg-5 col-md-5">
											<label for="">Formation</label>
											<input type="text" class="form-control"  name="category" required="required" value="<?php echo $row[0]->category;?>">
										</div>
										<div class="form-group col-lg-5  col-md-5">
											<label for=""><?php echo $this->lang->line('stock',FALSE);?></label>
											<input type="number" class="form-control"   name="amount" required="required" value="<?php echo $row[0]->size;?>" min="0">
										</div>
										<div class="form-group col-lg-3  col-md-3">
											<label for=""><?php echo $this->lang->line('item_cost',FALSE);?></label>
											<input type="number" step="0.01" class="form-control"   name="cost" required="required" value="<?php echo $row[0]->cost;?>">
										</div>
										<div class="form-group col-lg-3  col-md-3">
										Expire Date:	
										<div class="input-group date" data-provide="datepicker">
										    <input type="text" class="form-control" name="expire_date" required value="<?php echo $row[0]->expire_date;?>">
										    <div class="input-group-addon">
										        <i class="fa fa-calendar"></i>
										    </div>
										</div>
											</div>
										</div>

										</div>
										<!-- Price particulars -->
										<div class="panel panel-success">
											<div class="panel-heading"></div>
											<div class="panel-body">
												<div class="col-lg-12 col-md-12">
											<div class="form-group col-lg-4  col-md-4">
											<label for="">Cash Price</label>
											<input type="number" step="0.01" min="0" class="form-control"   name="cash" required="required" value="<?php echo $row[0]->price;?>">
										</div>
										<div class="form-group col-lg-4  col-md-4">
											<label for="">NHIF Price</label>
											<input type="number" step="0.01" min="0" class="form-control"   name="NHIF" required="required" value="<?php echo $row[0]->NHIF;?>">
										</div>
										<div class="form-group col-lg-4  col-md-4">
											<label for="">STRATEGY Price</label>
											<input type="number" step="0.01" min="0" class="form-control" name="STRATEGY" required="required" value="<?php echo $row[0]->STRATEGY;?>">
										</div>
										<div class="form-group col-lg-4  col-md-4">
											<label for="">TANESCO Price</label>
											<input type="number" step="0.01" min="0" class="form-control"   name="TANESCO" required="required" value="<?php echo $row[0]->TANESCO;?>">
										</div>
										<div class="form-group col-lg-4  col-md-4">
											<label for="">BUNGE Price</label>
											<input type="number" step="0.01" min="0" class="form-control"   name="BINGE" required="required" value="<?php echo $row[0]->BINGE;?>">
										</div>
										<div class="form-group col-lg-4  col-md-4">
											<label for="">Re-Order Level</label>
											<input type="number" step="0.01" min="0" class="form-control"   name="reorder_level" required="required" value="<?php echo $row[0]->reorder_level;?>">
										</div>
											
										</div>
											</div>
										</div>
										<!-- Buttons below-->
										<div class="col-lg-12  col-md-12">
											<button type="button" class="btn btn-danger" id="concel">Back</button>
											<button type="submit" class="btn btn-success" name="update_item" onclick="return confirm('Are You Sure?');">
											Update</button>	
										</div>
									
										<input type="hidden" name="item_id" required="required" value="<?php echo $row[0]->item_id;?>" />
									
								
		</form>
	 	</div>
	 </div>
	</div>
</div>
</div>
</div>