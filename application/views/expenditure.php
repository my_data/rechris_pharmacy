<script type="text/javascript">
$(document).ready(function() {
//filtering of drinks
$("#sortby").change(function(){
	window.location.href="<?php echo site_url('expenditure/filter/')?>"+this.value;
});
//searching of drinks
$("#search").keyup(function(){
	var value=this.value;
	if (value.length>4) {
		window.location.href="<?php echo site_url('expenditure/search/')?>"+this.value;
	}	
});
});
</script>
<div class="panel panel-default">
	<div class="panel-body">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Spoil Management</h3>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
		<table class="table  table-hover table-striped table-bordered">
			<thead>
				<tr>
					<th>
							<?php
					$attrib=array('class'=>'form-inline');
					 echo form_open('expenditure/search',$attrib);
					 ?>	
			    		<div class="input-group">
			    		<input class="form-control mr-sm-2" type="search" id="search" placeholder="Search" aria-label="Search" name="keyword" disabled />
 					</div>
 					</form>
					</th>
						
					<!-- Adding complementary drinks -->
					<th><a class="btn btn-primary pull-right" data-toggle="modal" href='#modal-id2'><i class="fa fa-plus"></i>&nbspComplementary</a>
					<div class="modal fade" id="modal-id2">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" style="text-align: center;">Complementary</h4>
								</div>
								<div class="modal-body">
									<?php
					$attrib=array('class'=>'form-inline');
					 echo form_open('expenditure/addcomplementary',$attrib);
					 ?>	
					 <div class="table-responsive">
					 	<table class="table table-hover">
					 		<thead>
					 			<tr>
					 				<th></th>
					 			</tr>
					 		</thead>
					 		<tbody>
					 			<tr>
					 			<td>Drug Name</td>
					 			<td><select name="drink_id" class="form-control input-sm" required="required">
					 				<?php $result=$this->db->get('drinks');
					 					$res=$result->result();
					 					foreach ($res as $drink) {
					 				?>
			    				<option value="<?php echo $drink->item_id;?>"><?php echo $drink->name.' /'.$drink->category;?> </option>
			    					<?php
			    					}
			    					?>
			    					</select>
			    				</td>
					 			</tr>
					 			<tr>
					 			<tr>
					 			<td><?php echo $this->lang->line('quantity',FALSE);?></td>
					 			<td><input type="number" name="quantity" class="form-control input-sm" min="1" required="required">
			    				</td>
					 			</tr>
					 			<tr>
					 			<td><?php echo $this->lang->line('receiver',FALSE);?></td>
					 			<td><input type="text" name="receiver" class="form-control input-sm" value="" required="required">
			    				</td>
					 			</tr>
					 			<tr><td colspan="5"><button type="submit" name="addcomplementary" class="btn btn-primary btn-block"><i class="fa fa-plus"></i></button></td></tr>
					 		</tbody>
					 	</table>
					 </div>
 					</form>
								</div>
								<div class="modal-footer">
									
								</div>
							</div>
						</div>
					</div></th>
					<!-- Returning spoil drinks -->
					<th>
						<a class="btn btn-warning pull-right btn-block" data-toggle="modal" href='#modal-id3'><i class="fa fa-plus"></i>&nbspSpoil</a>
					<div class="modal fade" id="modal-id3">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" style="text-align: center;">Spoil</h4>
								</div>
								<div class="modal-body">
									<?php
					$attrib=array('class'=>'form-inline');
					 echo form_open('expenditure/addSpoil',$attrib);
					 ?>	
					 <div class="table-responsive">
					 	<table class="table table-hover">
					 		<thead>
					 			<tr>
					 				<th></th>
					 			</tr>
					 		</thead>
					 		<tbody>
					 			<tr>
					 			<td>Drug Name</td>
					 			<td><select name="drink_id" class="form-control input-sm" required="required">
					 				<?php $result=$this->db->get('drinks');
					 					$res=$result->result();
					 					foreach ($res as $drink) {
					 				?>
			    				<option value="<?php echo $drink->item_id;?>"><?php echo $drink->name.' /'.$drink->category;?> </option>
			    					<?php
			    					}
			    					?>
			    					</select>
			    				</td>
					 			</tr>
					 			<tr>
					 			<tr>
					 			<td>Dose</td>
					 			<td><input type="number" name="quantity" class="form-control input-sm" min="1" required="required">
			    				</td>
					 			</tr>
					 			<tr><td colspan="5"><button type="submit" name="addspoil" class="btn btn-primary btn-block"><i class="fa fa-plus"></i></button></td></tr>
					 		</tbody>
					 	</table>
 					</form>
								</div>
							</div>
						</div>
					</div>
					</th>
				</tr>
			</thead>
		</table>
				</div>
			</div>
	</div>
</div>