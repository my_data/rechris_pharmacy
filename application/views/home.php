    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12"> 
                  <div class="panel panel-default box">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                        </div>
                        <div class="panel-body">
                                <span class="glyphicon glyphicon-usd" style="color:red;font-size:40px"></span>
                                &nbsp Today Sales:
                                <?php 
                                $time=date('Y-m-d',time());

                                if($query_str=$this->db->query("SELECT sum(price) as total_sale FROM drinkSales WHERE sale_time='$time'")){
                                        $query_sale=$query_str->result();
                                        echo number_format($query_sale[0]->total_sale)."/= Tshs";
                                }
                                ?>
                                
                            
                    </div>
                </div>
            </div>
             <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                    <div class="panel panel-default box">
                        <div class="panel-heading">
                        </div>
                        <div class="panel-body">
                        
                        <span class="glyphicon glyphicon-home" style="color:blue;font-size:40px"></span> Today Sold Items:
                           
                           <?php 
                           $time=date('Y-m-d',time());
                            if($query_str=$this->db->query("SELECT sum(amount) as sold_stock FROM drinksales WHERE sale_time='$time'")){
                                    $query_sale=$query_str->result();
                                    echo number_format($query_sale[0]->sold_stock);
                            }
                            ?>
                            
              
                        </div>
                    </div>
               
                </div>
            <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                   <div class="panel panel-warning box">
                        <div class="panel-heading">
                            
                        </div>
                        <div class="panel-body">
                           <span class="glyphicon glyphicon-home" style="color:red;font-size:40px"></span> Current Stock:
                            
                           <?php 
                            if($query_str_stock=$this->db->query("SELECT sum(size) as stock FROM drinks")){
                                    $stock=$query_str_stock->result();
                                    echo number_format($stock[0]->stock);
                            }
                            ?>
                    </div>
                </div>
            </div>
               <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                    <div class="panel panel-success box">
                        <div class="panel-heading">
                            
                        </div>
                        <div class="panel-body">
                       
                       <span class="glyphicon glyphicon-user" style="color:blue;font-size:40px"></span> System Users:
                    <?php 
                    if($query_str=$this->db->query("SELECT count(*) as rows FROM employees")){
                            
                            $query_str=$query_str->result();
                            echo  number_format($query_str[0]->rows);
                    }
                    ?>
                    
                    </div>
                </div>
            </div>
               
            </div>
            <div class="row" style="height:500px; margin-top:2%">
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                     <!-- <img src="<?php //echo base_url();?>images/drug.png" 
                     class="img-thumbnail img-fluid img-responsive" alt="Image" width="100%" height="236"> -->
                       <div class="box box-primary">
                        <div class="box-header with-border">
                          <h3 class="box-title">My Sales Per Month</h3>

                          <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                          </div>
                        </div>
                        <div class="box-body">
                          <div class="chart">
                            <canvas id="areaChart" style="height:400px"></canvas>
                          </div>
                        </div>
                        <!-- /.box-body -->
                      </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                    <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title">My Sales Per Month</h3>

                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div>
                    <div class="box-body">
                      <div class="chart">
                        <canvas id="lineChart" style="height:400px"></canvas>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 </div>

<!-- page script -->
