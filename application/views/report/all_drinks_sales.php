<div class="panel panel-default">
	<div class="panel-body">			
		<div class="panel panel-default">
		<div class="panel-heading">
			 All Drugs Sales
		</div>
			<div class="panel-body">
				  
				 <div class="table-responsive"> 
				 	<table class="table table-hover table-bordered" id="all-sales-tbl">
				 		<thead>
				 			<tr>
				 				<th>Drug Name</th><th>Formulation</th><th>Customer</th><th>Saler</th><th>Day</th><th>Quantity</th><th>Price(Tshs)</th><?php 
				 				if($this->employee->checkpermision(8)){	
					 			?><th>Action</th><?php } ?>
				 			</tr>
				 		</thead>
				 		<tbody>
				 		<?php 
				 $query_sales=$this->db->query("SELECT * FROM drinkSales inner join employees on saler_id=employee_id ORDER BY sale_time DESC");
				 if($query_sales->num_rows()>0){ 
				 	foreach ($query_sales->result() as $sale) {
				 		?>
				 		<tr>
				 			<?php
				 			$attrib=array('class'=>'form-inline','role'=>'form');
				 			echo form_open('drinksale/updateSale',$attrib);
				 			?>
				 			<td> 
				 			<?php echo $sale->name; ?></td>
				 			<td><?php echo $sale->category; ?></td>
				 			<td><input type="text" name="customer" class="form-control" 
				 			value="<?php echo $sale->customer; ?>" required="required"></td>
				 			<td><?php echo $sale->firstname.' '.$sale->lastname;?></td>
				 			<td><?php echo $sale->sale_time;?></td>
				 			<input type="hidden" name="old_sale_qty" value="<?php echo $sale->amount; ?>" required="required">
				 			<td>
				 				<input type="number" min="1" name="new_sale_qty" class="form-control input-sm" value="<?php echo $sale->amount; ?>" required="required" size="5">
				 			</td>
				 			<td>
					 			<?php echo number_format($sale->price); ?>/=
				 			</td>
				 			<input type="hidden" name="drink_id" value="<?php echo $sale->item_id; ?>" required="required">
				 			<input type="hidden" name="sale_id" value="<?php echo $sale->sale_id; ?>" required="required">
				 			<?php
					 			if($this->employee->checkpermision(8)){	
					 		?>
				 		<td>
							<div class="dropup">
							    <button class="btn btn-default dropdown-toggle btn-sm btn-block" type="button" data-toggle="dropdown" style="color:green">More...
							    </button>
							    <ul class="dropdown-menu">
							      <li>
								    <button type="submit" name="updateSale" class="btn btn-success btn-sm" onclick="return confirm('Do you want to Update?')">
								      Update
								  	</button>
								    <button type="submit" name="deleteSale" class="btn btn-danger btn-sm" onclick="return confirm('Do you want to delete?')">
								    	Delete
									</button>
							      </li>
							    </ul>
							</div>
						</td> 
				 			<?php 
				 			} ?>
				 			</form>
				 	</tr>
				 	<?php
				 			}} ?>
				 			
				 		</tbody>
				 	</table>
				 </div>
				 </div>
				 </div>
				 </div>
			</div>	
		</div>
	</div>		
</div>
	