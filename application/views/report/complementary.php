<div class="panel panel-primary">
	<div class="panel-heading">
		
	</div>
	<div class="panel-body">
	<div class="panel panel-default">
	<div class="panel-heading">
		Complementary Drinks
	</div>
		<div class="panel-body">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
		<?php 
		$attr=array('class'=>'form-inline','role'=>'form');
		echo form_open('reports/get_complementary',$attr);?>
		<div class="table">
			<table class="table table-hover">
				<thead>
					<tr>
						<th><div class="form-group">
						Today:<input type="checkbox" name="today" id="today" class="form-control input-sm">
						</div>
						</th>
						<th>
							<div class="input-group">
							From:<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="date" name="from" id="from" class="form-control input-sm">
							</div>
						</th>
						<th>
							<div class="input-group">
							To:<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="date" name="to" id="to" class="form-control input-sm">
							</div>
						</th>
						<th>
					<button type="submit" class="btn btn-primary btn-sm btn-block" name="show_complementary"><i class="fa fa-eye"></i>
						</button>
						</th>
					</tr>
				</thead>	
			</table>
		</div>
		</form>	
				<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th>Drink Name</th><th>Quantity</th><th>Receiver</th><th>Time</th><th>Employee</th>
					</tr>
				</thead>
				<tbody>
				<?php 
				if (isset($result)) {
					foreach ($result->result() as $v) {
						?>
					<tr>
						<td><?php echo $v->name; ?></td><td><?php echo $v->quantity; ?></td><td><?php echo $v->receiver; ?></td><td><?php echo $v->day; ?></td><td><?php echo $v->firstname.' '.$v->lastname; ?></td>
					</tr>
						<?php
					}
					?>
					<tr style="color: red;"><td>Total Drinks:</td><td><?php echo $v->qty; ?></td></tr>
					<?php
				}
				?>
				</tbody>
			</table>
		</div>
		</div>
	</div>
		
	</div>
</div>