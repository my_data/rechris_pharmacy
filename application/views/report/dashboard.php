<style type="text/css">
	.dropdown-submenu {
    position: relative;
}

.dropdown-submenu>.dropdown-menu {
    top: 0;
    left: 100%;
    margin-top: -6px;
    margin-left: -1px;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
    display: block;
}

.dropdown-submenu>a:after {
    display: block;
    content: " ";
    float: right;
    width: 0;
    height: 0;
    border-color: transparent;
    border-style: solid;
    border-width: 5px 0 5px 5px;
    border-left-color: #ccc;
    margin-top: 5px;
        margin-right: -10px;
}

.dropdown-submenu:hover>a:after {
    border-left-color: #fff;
}

.dropdown-submenu.pull-left {
    float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
    left: -100%;
    margin-left: 10px;
    -webkit-border-radius: 6px 0 6px 6px;
    -moz-border-radius: 6px 0 6px 6px;
    border-radius: 6px 0 6px 6px;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
    <div class="panel panel-default">
         <div class="panel-heading">
                <h3 class="panel-title">REPORTS CONSOLE</h3>
            </div>
        <div class="panel-body">
    <!-- Drob down menu for listing report options -->
    <div class="container">
    <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            
            <ul class="list-group">
             <li class="list-group-item">
             <a href="<?php echo site_url('reports/get_drinks_stock');?>" class="list-group-item">Inventory Summary</a>
             </li>
              <li class="list-group-item">
              <a href="<?php echo site_url('reports/drinkSales/summary');?>" class="list-group-item"> Sales Summary</a>
              </li>
                <li class="list-group-item">
                <a href="<?php echo site_url('reports/drinkSales/detailed');?>" class="list-group-item">Datailed Report</a>
                </li>
                <li class="list-group-item">
                <a href="<?php echo site_url('reports/get_spoil');?>" class="list-group-item">Spoiled  Drugs</a>
                </li>
                <li class="list-group-item">
                <a href="<?php echo site_url('reports/get_receiving');?>" class="list-group-item">Receiving</a>
                </li>
            </ul>
        </div>
        <!-- end of dropdown menu report for drinks -->
    </div>
        </div>
    </div>
	
</div>
