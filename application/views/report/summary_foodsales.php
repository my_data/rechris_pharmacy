<div class="panel panel-primary">
	<div class="panel-heading">
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
		<div class="well well-lg">
		<?php 
		$attr=array('class'=>'form-inline','role'=>'form');
		echo form_open('reports/sales/summary',$attr);?>
		<div class="form-group">
				<label  for="">Today</label>
				<input type="checkbox" name="today" id="today" class="form-control">
		</div><br/>From:
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="date" name="from" id="from" class="form-control">
			</div>To:
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="date" name="to" id="to" class="form-control">
			</div>
			<button type="submit" class="btn btn-primary" name="show_summary">Show</button>
		</form>	
		</div>
		<div class="panel panel-default">
		<?php require_once(APPPATH.'/views/print.php');?>
			<div class="panel-heading">
				<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<button type="button" onclick="printData('printArea')" class="btn btn-success col-md-4 col-lg-4 col-sm-4 pull-right"><i class="fa fa-print"></i></button>
			</div>
			</div>
			</div>
			<div class="panel-body" id="printArea">
			<div class="table-responsive">
				 	<table class="table table-hover">
				 		<thead>
				 			<tr>
				 				<th><?php echo $this->lang->line('food_name',FALSE);?></th><th><?php echo $this->lang->line('category',FALSE);?></th><th><?php echo $this->lang->line('sold_quantity',FALSE);?></th><th><?php echo $this->lang->line('cash',FALSE);?></th>
				 			</tr>
				 		</thead>
				 		<tbody>
				<?php
				if (isset($cash) && isset($sold_item_name)){
					$total_sale=$cash->row();
					foreach ($sold_item_name->result() as $item) {
					$res=get_sold_item_qty($item->name,$this->session->userdata('from'),$this->session->userdata('to'));
					 foreach ($res as $qty) {
						$r=get_sold_item_cash($item->name,$this->session->userdata('from'),$this->session->userdata('to'));
					 	foreach ($r as $money) {
					?>
				 		
				 			<tr><td><?php echo $item->name;?></td><td><?php echo $item->category;?></td><td><?php echo $qty->qty;?></td><td><?php echo number_format($money->cash);?></td></tr>
				 		
					<?php
				}}}
				
				?>
				<tr style="color: red;">
				 	<th colspan="3">Total Cost:</th><th colspan="3"><?php echo number_format(get_sold_items_cost());

				 	$this->session->set_userdata('total_cost',get_sold_items_cost());?></th>
				 </tr>
				<tr style="color: green;">
				 	<th colspan="3">Gross Profit :</th><th colspan="3"><?php echo number_format($total_sale->value_sum); 

				 	$this->session->set_userdata('gross_profit',$total_sale->value_sum);?></th>
				 </tr>
				 <tr style="color: green;">
				 	<th colspan="2">Net Profit:</th><th colspan="2"><h3><?php echo number_format(($total_sale->value_sum-get_sold_items_cost()));?></h3></th>
				 </tr>
				<?php 

				}
				else{
						?><tr><td><div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							 Select Record!
						</div></td></tr><?php
					
					} ?>
				</tbody>
				</table>
				 </div>
			</div>	
		</div>
	</div>
</div>