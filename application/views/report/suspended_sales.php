<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/datatables.min.css"/>
<div class="panel panel-default">
			<div class="panel-heading"></div>
			<div class="panel-body">	
			<div class="col-md-12 col-lg-12 col-sm-12">
			<?php require_once(APPPATH.'/views/print.php');?>
			<button type="button" onclick="printData('printArea')" class="btn btn-success btn-sm">
				<i class="fa fa-print"></i>
			</button>
			</div>
				<h3 style="text-align: center;">INSURANCE SALES</h3>
				 		 			
			<div class="table-responsive" id="printArea">
				 	<table class="table table-hover table-striped table-bordered" id="mytable1">
				 		<thead>
				 			<tr>
				 				<th>NAME</th><th>CATEGORY</th><th>QUANTITY</th><th>SOLD DATE</th><th>PATIENT</th><th>CASH</th><th>SOLD BY EMPLOYEE</th><th>...</th>
				 			</tr>
				 		</thead>
				 		<tbody>
				<?php

				$result_obj=$this->db->query("SELECT * FROM drinkSales INNER JOIN employees ON drinkSales.saler_id=employees.employee_id WHERE payment_type!='Cash'");

				if ($result_obj) {
					$total_sales=0;
					if ($result_obj->num_rows()<1){
							?>
				 		<tr><td colspan=""><div class="alert alert-danger" style="text-align: center;">
				 			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				 			No Records Found!
				 		</div></td></tr>
				
				 <?php
						}else{
					foreach ($result_obj->result() as $item) {
						$total_sales=$total_sales+($item->price*$item->amount);
					?>
				 		<tr><td><?php echo $item->name;?></td><td><?php echo $item->category;?></td><td><?php echo $item->amount;?></td><td><?php echo $item->sale_time;?></td>
				 			<td><?php echo $item->customer;?></td><td><?php echo $item->price;?></td><td><?php echo $item->firstname.' '.$item->lastname;?></td><td><a href="<?php echo site_url('reports/unsuspend/'.$item->sale_id);?>" class="btn btn-success">Sale</a></td></tr>
				 		
					<?php
				}
			}
				?>
				<?php }
				?>
				<tr style="color:green;"><td>Total INSURANCE Sales</td><td></td><td></td><td></td><td></td><td></td><td><?php echo $total_sales;?></td><td></td></tr>
				</tbody>
				</table>
		</div>
		</div>
		<script type="text/javascript" src="<?php echo base_url();?>datatable/datatables.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#mytable1").DataTable();
			});
		</script>
</div>