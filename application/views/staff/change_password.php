<script type="text/javascript">
	$(document).ready(function(){
		$("#concel").click(function(){
			window.location.href="<?php echo site_url('home');?>";
		});

	});
</script>
<style type="text/css">
	h6{
		text-align: center;
	}
	h5{
		text-align: center;text-transform: uppercase;text-shadow:0.5px 0.5px 0.5px black;
	}
	input{
		text-align: center;
	}
</style>
<div class="col">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Change Password</h3>
			</div>
			<div class="panel-body">
			<?php echo form_open('staff/update_password');?>
				<div class="form-group col-lg-6 col-md-6">
				<label for="">Old Password</label>
				<input type="password" class="form-control input-sm"  name="old_password" required="required"/>
				</div>
				<div class="form-group col-lg-6 col-md-6">
				<label for="">New Password</label>
				<input type="password" class="form-control input-sm"  name="new_password" required="required"/>
				</div>
				<button type="button" class="btn btn-danger btn-sm" id="concel">Back</button>
				<button type="submit" class="btn btn-success btn-sm" name="update_password">Update</button>
			</form>
			</div>
		</div>
	</div>
</div>