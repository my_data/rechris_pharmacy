<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/datatables.min.css"/>
<div class="row">
	<div class="col-lg-10 col-sm-10 col-md-10 col-xs-12">
<div class="panel panel-default">
	<div class="panel-body">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><i class=" fa fa-user"></i>&nbsp Users</h3>
		</div>
		<div class="panel-body">
	<div class="table-responsive">		
	<table class="table table-responsive">
		<thead>
			<tr>
					<th>
					<div>
					<a class="btn btn-default btn-sm pull-right" data-toggle="modal" href='#modal-id'>
						<i class=" fa fa-plus"></i>&nbsp<i class=" fa fa-user"></i>&nbsp User
					</a>
					<div class="modal fade" id="modal-id">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title"><?php echo $this->lang->line('new_user',FALSE);?></h4>
								</div>
								<div class="modal-body">
									<?php 
									$attrib=array('role'=>'form','class'=>'form');
									echo form_open('staff/save',$attrib);
									?>
									<div class="table-responsive">
										<table class="table table-striped table-bordered">
											<tbody>
												<tr>
													<td>
														<div class="form-group">
											<label for=""><?php echo $this->lang->line('first_name',FALSE);?></label>
											<input type="text" class="form-control input-sm"  name="firstname" required="required" id="firstname">
										</div></td><td><div class="form-group">
											<label for=""><?php echo $this->lang->line('surname',FALSE);?></label>
											<input type="text" class="form-control input-sm"  name="lastname" required="required">
										</div></td>
												</tr>
												<tr><td><div class="form-group">
											<label for=""><?php echo $this->lang->line('username',FALSE);?></label>
											<input type="text" class="form-control input-sm"   name="username" required="required">
										</div></td><td><div class="form-group">
											<label for=""><?php echo $this->lang->line('password',FALSE);?></label>
											<input type="password" class="form-control input-sm"   name="password" required="required">
										</div></td>
										</tr>
										<tr>
											<td colspan="2">
												<?php echo $this->lang->line('permissions',FALSE);?></td>
											</tr>
										<tr style="color: green;">
											<td colspan="2">
											<?php
											$counter=0;
											$modules=$this->module->get_all_modules();
											 foreach($modules as $row){
											 	$counter++;
											 ?>
											 <div class="form-group col-lg-3 col-sm-5 col-md-3">
								<label for="<?php echo $row->classname;?>">
								<?php echo $row->classname;?>
								</label>
						<input type="checkbox" class="form-control"   name="module<?php echo $counter;?>" value="<?php echo $row->module_id;?>">
										</div>
											<?php
											}?>
								<input type="hidden" name="total_modules" value="<?php echo $counter;?>">
									
										</td>
									</tr>
										<tr>
											<td colspan="2">
												<button type="submit" name="save_staff" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add</button>
											</td></tr>
											</tbody>
										</table>	
									</form>
								</div>
							</div>
						</div>
					</div>
				</div></th>
				</tr>
		</thead>
		
	</table>
</div>
<div class="table-responsive">
		<table class="table table-bordered" id="mytable1">
		<thead>
			<tr><th></th><th><?php echo $this->lang->line('first_name',FALSE);?></th><th><?php echo $this->lang->line('surname',FALSE);?></th><th><?php echo $this->lang->line('username',FALSE);?></th><th><?php echo $this->lang->line('action',FALSE);?></th></tr>
		</thead>
			<tbody>
			
				<?php
				if(isset($allstaff)){
					$counter=0;
				foreach ($allstaff as $row) {
					$counter++;
					?>
				<tr>
					<td><?php echo $counter;?></td>
					<td><?php echo $row->firstname;?></td>
					<td><?php echo $row->lastname;?></td>
					<td><?php echo $row->username;?></td>
					<td colspan="2">
						<a href="<?php echo site_url('staff/update/'.$row->employee_id);?>">
							<i class="fa fa-edit" style="color:green"></i>
						</a>&nbsp &nbsp
						<a href="<?php echo site_url('staff/delete/'.$row->employee_id);?>" onclick="return confirm('Are you sure you want to delete this User?');">
							<i class="fa fa-trash-o" style="color:red"></i>
						</a>
					</td>
				</tr>
					<?php
				}
				}
				else{
					?>
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->lang->line('error_sms',FALSE);?>
						
					</div>
					<?php
				}
				?>
			</tbody>
			<tfoot>
				
			</tfoot>
		</table>
	</div>
	</div>
	</div>
	</div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>datatable/datatables.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#mytable1").DataTable({
					dom:'Bfrtip',
					        buttons: [
					            'excelHtml5',
					            'csvHtml5',
					            'pdfHtml5'
					        ],
					         responsive: true
				});
			});
		</script>
