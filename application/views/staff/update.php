<script type="text/javascript">
	$(document).ready(function(){
		$("#cancel").click(function(){
			window.location.href="<?php echo site_url('staff');?>";
		});

	});
</script>
<style type="text/css">
	input{
		text-align: center;
	}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Edit User</h3>
			</div>
			<div class="panel-body">
		<?php echo form_open('staff/update');?>
		<div class="table-responsive">
			<table class="table table-bordered">
				<tbody>
					<tr>
						<td><div class="form-group">
			<label for=""><?php echo $this->lang->line('first_name',FALSE);?></label>
			<input type="text" class="form-control input-sm"  name="firstname" required="required" value="<?php echo $staff[0]->firstname;?>" />
			</div></td>
			<td><div class="form-group">
			<label for=""><?php echo $this->lang->line('surname',FALSE);?></label>
			<input type="text" class="form-control input-sm"  name="lastname" required="required" value="<?php echo $staff[0]->lastname;?>" />
			</div></td>
					</tr>
					<tr><td><div class="form-group">
			<label for=""><?php echo $this->lang->line('username',FALSE);?></label>
			<input type="text" class="form-control input-sm"   name="username" required="required" value="<?php echo $staff[0]->username;?>" />
			</div></td><td>	<div class="form-group">
		<label for="" style="color: red;">Reset Password</label>
		<input type="password" class="form-control input-sm" name="password" placeholder="Enter New password" />
		<input type="hidden"  name="employee_id" required="required" value="<?php echo $staff[0]->employee_id;?>" />
		</div></td>
			</tr>
			<tr>
			<td colspan="2"><strong><?php echo $this->lang->line('permissions',FALSE);?></strong>
			</td>
			</tr>
			<tr style="color: green;">
			<td colspan="2">
						<?php
							$counter=0;
							$status='';
							$permission_status='';
							$modules=$this->db->get('modules');
							foreach($modules->result() as $row){
									$counter++;
									if($this->employee->has_permission($row->module_id,$staff[0]->employee_id)){
										$permission_status="checked";
									}
									if(!$this->employee->has_permission($row->module_id,$staff[0]->employee_id)){
										$permission_status=" ";
									}
						 ?>
						<div class="checkbox col-lg-3 col-sm-3 col-md-3 col-xs-12">
							
						<label>
						<input type="checkbox" name="module<?php echo $counter;?>" value="<?php echo $row->module_id;?>" <?php echo $permission_status;?> ><?php echo $row->classname;?><?php echo $row->classname;?>
						</label>
					
						</div>
						<?php
						}
						?>
								<input type="hidden" name="total_modules" value="<?php echo $counter;?>">
									
				</td>
				</tr>
			<tr><td><button type="button" class="btn btn-danger btn-block" onclick="back()">Back</button></td>
			<td><button type="submit" class="btn btn-primary btn-block" name="update_staff">Update</button></td>
			</tr>
				</tbody>
			</table>
		</div>
		</form>
		</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function back(){
		window.location.href="<?php echo site_url('staff');?>";
	}
</script>