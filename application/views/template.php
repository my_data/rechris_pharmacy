<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>template/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>template/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>template/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>template/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>template/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url();?>template/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url();?>template/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>template/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>template/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url();?>template/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!-- old css and javascripts -->
<!-- css links -->
<link href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/datatables.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/Responsive-2.2.2/responsive.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/Responsive-2.2.2/responsive.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/Responsive-2.2.2/responsive.foundation.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/Responsive-2.2.2/responsive.jqueryi.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/Buttons-1.5.6/css/buttons.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/Buttons-1.5.6/css/buttons.dataTables.min.css"/>
<link href="<?php echo base_url(); ?>css/styles.css" rel="stylesheet" type="text/css" />
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper fixed">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo site_url('home');?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>P</b>OS</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Pharmacy</b></span>
    </a>
        <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url();?>template/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->userdata('username');?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url();?>template/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  <?php echo $this->session->userdata('username');?>
                  <small>Member since  2019</small>
                </p>
              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo site_url('staff/update_password')?>" class="btn btn-default btn-flat">Change Password</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo site_url('home/Logout')?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url();?>template/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('username');?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <!-- <li class="header">...</li> -->
        <ul class="nav nav-pills nav-stacked" >
            <li>
              <a href="<?php echo site_url('home')?>" style="color:white; text-decoration: none;">
                <i class="fa fa-dashboard fa-2x"></i>&nbsp<?php echo $this->lang->line('dashboard',FALSE);?>
              </a>
            </li>
            <?php 
            $counter=1;
            foreach($allowed_modules->result() as $module){
            $image=base_url()."images/".$counter.".png";
              ?>
            <li>
              <a href="<?php echo site_url('home/get_module/'.$module->module_id);?>">
              <span>
              <img src="<?php echo $image;?>" class="img-responsive img-circle img-fluid" alt="Image" style="width:20%;">
              <?php echo $module->classname;?> &nbsp 
              <i class="arrow fa fa-angle-right"></i></a>
              </span>
              
            </li>
              <?php
              $counter++;
            }
            ?>
              
            </ul>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content">
     <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index:3;">
          <?php require_once('success_info.php');?>
          <?php require_once('fail_info.php');?>
       </div>
            <?php $this->load->view($page);?>
          </div>
     </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.1
    </div>
    <strong>Copyright &copy; 2019<a href="#">+255-689-843-428</a>.</strong> All rights
    reserved.
  </footer>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>template/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url();?>template/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>template/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url();?>template/bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url();?>template/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>template/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>template/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>template/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>template/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url();?>template/bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>template/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>template/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>template/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>template/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>template/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url();?>template/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>template/dist/js/demo.js"></script>
<!-- javascripts links -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/print.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>print_plugin/jQuery.print.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>datatable/Responsive-2.2.2/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>datatable/Responsive-2.2.2/responsive.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>datatable/pdfmake/pdfmake.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>datatable/pdfmake/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>datatable/Buttons-1.5.6/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>datatable/Buttons-1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>datatable/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>datatable/Buttons-1.5.6/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>datatable/Buttons-1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>datatable/datatables.min.js"></script>
<script src="<?php echo base_url();?>template/bower_components/chart.js/Chart.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#mytable1").DataTable(
            {
                  dom: 'Bfrtip',
                  buttons: [
                      {
                          extend: 'csvHtml5',
                          exportOptions: {
                              columns: [0,1,2,3,5,6,7,8,9,10]
                          }
                      },
                      {
                          extend: 'pdfHtml5',
                          exportOptions: {
                              columns: [0,1,2,3,5,6,7,8,9,10]
                          }
                      },
                      {
                        extend: 'print',
                        exportOptions: {
                              columns: [0,1,2,3,5,6,7,8,9,10]
                          }
                      }
                     
                  ],
                  responsive:true
              }
          );

      });
    </script>
      <script type="text/javascript">
      $(document).ready(function(){
        $("#mytable2").DataTable(
            {
                  dom: 'Bfrtip',
                  buttons: [
                      {
                          extend: 'copyHtml5',
                          exportOptions: {
                              columns: [ 0, 1, 2, 5,6]
                          }
                      },
                      {
                          extend: 'csvHtml5',
                          exportOptions: {
                              columns: [ 0, 1, 2, 5,6]
                          }
                      },
                      {
                          extend: 'pdfHtml5',
                          exportOptions: {
                              columns: [ 0, 1, 2, 5,6]
                          }
                      }
                     
                  ]
              }
          );
      });
    </script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#alert-box").slideUp(10000);
  });
                
  </script>
  <script type="text/javascript">
$(document).ready(function(){
    $("#saleeee-form").submit(function(){
         $("#printArea").print({
                    globalStyles: true,
                    mediaPrint: false,
                    stylesheet: null,
                    noPrintSelector: ".no-print",
                    iframe: false,
                    prepend:"<h4>TIN:#</h4><table border='1'><tr><td>129</td><td>---</td><td>483</td><td>---</td><td>091</td></tr></table><br/>",
                    append:"<br/><h4>ADDRESS: Sido street,Block N, Plot # 08, Dodoma</h4><br/><h4>PHONE#: 0719899080, 0759013529</h4><h3 align='center'>Karibu tena!!</h3>",
                    manuallyCopyFormValues: true,
                    deferred: $.Deferred(),
                    timeout: 0,
                    title: "RECHRIS PHARMACY(Receipt)",
                    doctype: '<!doctype html>'
            });

});
$("#printProformer").click(function(){
         $("#printArea").print({
                    globalStyles: true,
                    mediaPrint: false,
                    stylesheet: null,
                    noPrintSelector: ".no-print",
                    iframe: false,
                    prepend:"<h4>TIN:#</h4><table border='1'><tr><td>129</td><td>---</td><td>483</td><td>---</td><td>091</td></tr></table><br/>",
                    append:"<br/><h4>ADDRESS: Sido street,Block N, Plot # 08, Dodoma</h4><br/><h4>PHONE#: 0719899080, 0759013529</h4><h3 align='center'>Karibu tena!!</h3>",
                    manuallyCopyFormValues: true,
                    deferred: $.Deferred(),
                    timeout: 0,
                    title: "RECHRIS PHARMACY(Proformer)",
                    doctype: '<!doctype html>'
            });

});
 });   
</script>
  <script type="text/javascript">
      $(document).ready(function(){
        $("#all-sales-tbl").DataTable(
            {
                  dom: 'Bfrtip',
                  buttons: [
                   
                      {
                          extend: 'csvHtml5',
                          exportOptions: {
                              columns: [ 0, 1, 2, 5,6]
                          }
                      },
                      {
                          extend: 'pdfHtml5',
                          exportOptions: {
                              columns: [ 0, 1, 2, 5,6]
                          }
                      },
                      {
                        extend: 'print',
                        exportOptions: {
                              columns: [0,1,2,5,6]
                          }
                      }
                     
                  ]
              }
          );

      });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
          $("#detailed-sale").slideUp();
          $("#summary-table").slideUp();
        });
</script>
     <script type="text/javascript">
          $(document).ready(function(){
            $("#toggle-table-summary").click(function(){
              $("#summary-table").slideToggle();
              $("#detailed-sale").slideUp();
            });
          });
         </script>
       <script type="text/javascript">
          $(document).ready(function(){
            $("#toggle-table-detail").click(function(){
              $("#detailed-sale").slideToggle();
              $("#summary-table").slideUp();
            });

            $("#toggle-table-summary").click(function(){
              $("#detailed-sale").slideUp();
            });
          });
      </script>
      <script type="text/javascript">
        var sales_data;
         $.ajax({url: "<?php echo site_url('home/sales_per_month');?>",
                type:'get',
                success: function(result){
                               sales_data= JSON.parse(result);
                                //alert(sales_data.jan);
                        }
        });
  $(function () {
           
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $('#areaChart').get(0).getContext('2d')
    // This will get the first returned node in the jQuery collection.
    var areaChart       = new Chart(areaChartCanvas)

    var saledata=[sales_data.jan,sales_data.feb,sales_data.mar,sales_data.ap,sales_data.may,sales_data.june,sales_data.jul,sales_data.aug,sales_data.sept,sales_data.oct,sales_data.nov,sales_data.dec]

    var areaChartData = {
      labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July','Aug','Sept','Oct','Nov','Des'],
      datasets: [
        {
          label               : 'Months',
          fillColor           : 'rgba(210, 214, 222, 1)',
          strokeColor         : 'rgba(210, 214, 222, 1)',
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : saledata
        },
        {
          label               : 'Total Sales',
          fillColor           : 'rgba(60,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : saledata
        }
      ]
    }

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale               : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - Whether the line is curved between points
      bezierCurve             : true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension      : 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot                : false,
      //Number - Radius of each point dot in pixels
      pointDotRadius          : 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth     : 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius : 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke           : true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth      : 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill             : true,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio     : true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive              : true
    }

    //Create the line chart
    areaChart.Line(areaChartData, areaChartOptions)

    //-------------
    //- LINE CHART -
    //--------------
    var lineChartCanvas          = $('#lineChart').get(0).getContext('2d')
    var lineChart                = new Chart(lineChartCanvas)
    var lineChartOptions         = areaChartOptions
    lineChartOptions.datasetFill = false
    lineChart.Line(areaChartData, lineChartOptions)

    //-------------
  })
</script>
</body>
</html>
